<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=hasil.xlsx");
?>
	<table border="1">
        <thead>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Age</th>
                <th>Start date</th>
                <th>Salary</th>
            </tr>
        </thead>
            <tr>
                <td><p style="color: rgb(68, 68, 68); background-color: rgb(249, 249, 249);">Tidak masuk dalam kepesertaan program-program pemerintah.</p><ol><li style="color: rgb(68, 68, 68); background-color: rgb(249, 249, 249);"><span style="color: rgb(68, 68, 68);">1. Kartu Keluarga Sejahtera (KKS)/ Kartu Perlindungan Sosial (KPS)</span></li><li style="color: rgb(68, 68, 68); background-color: rgb(249, 249, 249);">2. Kartu Indonesia Pintar (KIP)/ Bantuan Siswa Miskin (BSM)</li><li style="color: rgb(68, 68, 68); background-color: rgb(249, 249, 249);">3. Kartu Indonesia Sehat (KIS)/ BPJS Kesehatan PBI/ Jamkesmas</li><li style="color: rgb(68, 68, 68); background-color: rgb(249, 249, 249);">4. BPJS Kesehatan Peserta Mandiri</li><li style="color: rgb(68, 68, 68); background-color: rgb(249, 249, 249);">5. Program Keluarga Harapan (PKH)</li><li style="color: rgb(68, 68, 68); background-color: rgb(249, 249, 249);">6. Beras untuk Orang Miskin (Raskin)/Rastra/BPNT
                </li></ol>
                </td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>2011/04/25</td>
                <td>$320,800</td>
            </tr>
            <tr>
                <td>Garrett Winters</td>
                <td>Accountant</td>
                <td>Tokyo</td>
                <td>63</td>
                <td>2011/07/25</td>
                <td>$170,750</td>
            </tr>
            <tr>
                <td>Ashton Cox</td>
                <td>Junior Technical Author</td>
                <td>San Francisco</td>
                <td>66</td>
                <td>2009/01/12</td>
                <td>$86,000</td>
            </tr>
            <tr>
                <td>Cedric Kelly</td>
                <td>Senior Javascript Developer</td>
                <td>Edinburgh</td>
                <td>22</td>
                <td>2012/03/29</td>
                <td>$433,060</td>
            </tr>
            <tr>
                <td>Airi Satou</td>
                <td>Accountant</td>
                <td>Tokyo</td>
                <td>33</td>
                <td>2008/11/28</td>
                <td>$162,700</td>
            </tr>
            <tr>
                <td>Brielle Williamson</td>
                <td>Integration Specialist</td>
                <td>New York</td>
                <td>61</td>
                <td>2012/12/02</td>
                <td>$372,000</td>
            </tr>
            <tr>
                <td>Herrod Chandler</td>
                <td>Sales Assistant</td>
                <td>San Francisco</td>
                <td>59</td>
                <td>2012/08/06</td>
                <td>$137,500</td>
            </tr>
            <tr>
                <td>Rhona Davidson</td>
                <td>Integration Specialist</td>
                <td>Tokyo</td>
                <td>55</td>
                <td>2010/10/14</td>
                <td>$327,900</td>
            </tr>
            <tr>
                <td>Colleen Hurst</td>
                <td>Javascript Developer</td>
                <td>San Francisco</td>
                <td>39</td>
                <td>2009/09/15</td>
                <td>$205,500</td>
            </tr>
            <tr>
                <td>Sonya Frost</td>
                <td>Software Engineer</td>
                <td>Edinburgh</td>
                <td>23</td>
                <td>2008/12/13</td>
                <td>$103,600</td>
            </tr>
            <tr>
                <td>Jena Gaines</td>
                <td>Office Manager</td>
                <td>London</td>
                <td>30</td>
                <td>2008/12/19</td>
                <td>$90,560</td>
            </tr>
            <tr>
                <td>Quinn Flynn</td>
                <td>Support Lead</td>
                <td>Edinburgh</td>
                <td>22</td>
                <td>2013/03/03</td>
                <td>$342,000</td>
            </tr>
	</table>
