  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Quesioner Pelatihan <?=$detail->nama_pelatihan?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Update Quesioner Pelatihan <?=$detail->nama_pelatihan?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      

    <div class="col-md-12" >
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('kelolapelatihan/aksi_tambah_quesioner');  ?>
              <div class="box-body" >
              

              <input type="hidden" name="id_pelatihan" value="<?=$this->uri->segment(3)?>">
              <table style="width:100%" id="" class="table table-bordered table-striped" hidden>
                <thead>
                <tr>
                  <th>No</th>
                  <th>Pertanyaan</th>
                  <th><input type="checkbox" onclick="toggle(this);" /></th>
                  
                </tr>
                </thead>
                <tbody>
                <?php 
                $no = 0;
                foreach($quesioner as $all): 
                $no++;
                ?>
               
                  <td><?=$no?></td>
                  <td>
                  <input name="id_quesioner[]" value="<?=$all->id?>">
                  <?=$all->pertanyaan?></td>
                 
                  
                  <td>

                  <div class="checkbox">

                  
                  

                  <label>
                    <input name="quesioner[]"  type="checkbox" checked> 
                  </label>


                  <!-- <select name="persyaratan[]" class="form-control" required data-placeholder="Pilih Persyaratan" style="width: 100%;">
                  
                  <option <?=$sel?> value="1">Ya</option>
                  <option <?=$sel?> value="0">Tidak</option>
                  
                  </select> -->

                  </td>
                 
                  
            
                </tr> 
                <?php endforeach;?>
               
              </tbody>
              </table>
            
              
               
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                Belum ada data persyaratan, tambahkan sekarang ?
                <button type="submit" class="btn btn-info pull-right">Tambah Persyaratan</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>


  <?php include(__DIR__ . "/../template/footer.php"); ?>

  <script>
  function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}
</script>