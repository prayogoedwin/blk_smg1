  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Cetak Peserta Pelatihan <?=$detail->nama_pelatihan?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12 col-xs-12">

        <div class="box">
        <div class="box-header">
         <!-- <a href="<?=base_url()?>kelolapelatihan/tambah"><button type="button" class="btn btn-sm btn-primary">Tambah Data</button></a>  -->
         <!--<a onclick="return confirm('Anda Ingin Non Aktifkan Semua User ?')" href="<?=base_url()?>kelolauser/close_masal"><button type="button" class="btn btn-sm btn-danger">Non Aktifkan Massal</button></a> -->
         <!--<a onclick="return confirm('Anda Ingin Aktifkan Semua User ?')" href="<?=base_url()?>kelolauser/open_masal"><button type="button" class="btn btn-sm btn-success">Aktifkan Massal</button></a> -->
        </div>
        
            <!-- /.box-header -->
            <div class="box-body">
              <table style="width:100%" id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>NIK</th>
                  <th>Domisili</th>
                  <th>HP</th>
                  <th>WA</th>
                  <?php 
                  foreach($quesioner as $jwb):?>
                  <th><?=$jwb->pertanyaan?></th>
                  <?php endforeach; ?>
                
                
                 
                 
                  
                </tr>
                </thead>
                <tbody>
                <?php 
                $no = 0;
                foreach($alldata as $all): 
                $no++;
                ?>
               
                  <td><?=$no?></td>
                  <td><?=$all->nama?></td>
                  <td><?=$all->nik?></td>
                  <td><?=$all->kota?></td>
                  <td><?=$all->hp?> </td>
                  <td><?=$all->wa?> </td>

                  

                  <?php 
                  $ay = detail_jawaban_quesioner_by_pelatihan($detail->id, $all->id_user);
                  foreach($ay as $as):?>
           

                  <?php if($as->tipe_jawaban == 1){ ?>
                    <td><?=jawaban_pilihan($as->jawaban)?></td>   
                  <?php }else{ ?>

                    <?php if($as->tipe_quesioner != 5 ){ ?>
                    <td><?=$as->jawaban?></td>
                    <?php }else{ ?>
                      <td><?=base_url('file/'.$as->jawaban)?></td>
                    <?php } ?>

                  <?php } ?>

                  


                  <?php endforeach; ?>

                  
                 

                  

                  
            
                </tr> 
                <?php endforeach;?>
               
              </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->



        </div>
    </div>
    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>
  <?php 
  $no = 0;
  foreach($alldata as $all): 
  $no++;
  ?>
                <script>
                  $(document).ready(function(){ 
                    $('#quesioner<?=$all->id?>').tooltip();
                    $('#pendaftar<?=$all->id?>').tooltip();
                    $('#edit<?=$all->id?>').tooltip();
                    $('#hapus<?=$all->id?>').tooltip();
                  });
                  </script>
  <?php endforeach;?>

 


  <script>
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                title: 'Cetak Pendaftar Pelatihan <?=$detail->nama_pelatihan?>',
                
            }
            // {
            //     extend: 'pdfHtml5',
            //     title: 'Data export'
            // }
        ],
        // buttons: [
        //     //'copyHtml5',
        //     'excelHtml5',
        //     //'csvHtml5',
        //     //'pdfHtml5'
        // ],
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true,
      "fixedHeader": true
    });

</script>