  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Detail Pendaftar Pelatihan <?=$detail->nama_pelatihan?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Pelatihan <?=$detail->nama_pelatihan?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      
<div class="row">
    <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Biodata</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart();  ?>
            <div class="row">
              <div class="col-md-8">
                  <div class="box-body">

                  <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">Nama</label>
                      <div class="col-sm-8">
                        <input type="text" disabled  class="form-control" required="required" value="<?=$data_pendaftar->nama?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">NIK</label>
                      <div class="col-sm-8">
                        <input type="text" disabled  class="form-control" required="required" value="<?=$data_pendaftar->nik?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">Tempat Lahir</label>
                      <div class="col-sm-8">
                        <input type="text" disabled  class="form-control" required="required" value="<?=$data_pendaftar->tempat_lahir?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">Tanggal Lahir</label>
                      <div class="col-sm-8">
                        <input type="text" disabled  class="form-control" required="required" value="<?=$this->formatter->getDateMonthFormatUser($data_pendaftar->tgl_lahir)?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">Pendidikan Terakhir</label>
                      <div class="col-sm-8">
                        <input type="text" disabled  class="form-control" required="required" value="<?=pendidikan($data_pendaftar->pendidikan_terakhir)?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">Domisili</label>
                      <div class="col-sm-8">
                        <input type="text" disabled  class="form-control" required="required" value="<?=$data_pendaftar->kota?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">Nomor HP</label>
                      <div class="col-sm-8">
                        <input type="text" disabled  class="form-control" required="required" value="<?=$data_pendaftar->hp?>">
                      </div>
                    </div>


                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">Nomor Whatsapp</label>
                      <div class="col-sm-8">
                        <input type="text" disabled  class="form-control" required="required" value="<?=$data_pendaftar->wa?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">Gender</label>
                      <div class="col-sm-8">
                        <input type="text" disabled  class="form-control" required="required" value="<?=$data_pendaftar->gender?>">
                      </div>
                    </div>


                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">Golongan Darah</label>
                      <div class="col-sm-8">
                        <input type="text" disabled  class="form-control" required="required" value="<?=$data_pendaftar->golongan_darah?>">
                      </div>
                    </div>

                  
                  </div>
              </div>

              <div class="col-md-4">
              <img width="50%" src="<?=base_url($data_pendaftar->foto)?>">
              </div>


              </div>
              <!-- /.box-body -->
              <!-- <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Tambah</button>
              </div> -->
              <!-- /.box-footer -->
              <?php echo form_close(); ?>

          </div>



          <!-- //form quesioner -->
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Data Quesioner</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart();  ?>
              <div class="box-body">

              <?php foreach($jawaban_quesioner as $jwb): ?>


                

                <?php if($jwb->tipe_jawaban == 1): ?>

                  <div class="form-group" style="padding: 30px 0px 30px 0px;">
                    <label for="inputEmail3" class="col-sm-12 control-label"><?=$jwb->pertanyaan?></label>
                    <div class="col-sm-12">
                      <input type="text" disabled  class="form-control" required="required" value="<?=jawaban_pilihan($jwb->jawaban)?>">
                    </div>
                  </div>
                
                <?php endif; ?>

           

              <?php if($jwb->tipe_jawaban == 0 || $jwb->tipe_jawaban == 2): ?> 

                <?php if($jwb->tipe_quesioner != 5 ){ ?>

                <div class="form-group" style="padding: 30px 0px 30px 0px;">
                  <label for="inputEmail3" class="col-sm-12 control-label"><?=$jwb->pertanyaan?></label>
                  <div class="col-sm-12">
                    <input type="text" disabled  class="form-control" required="required" value="<?=$jwb->jawaban?>">
                  </div>
                </div>

                <?php }else{ ?>

                  <div class="form-group" style="padding: 30px 0px 30px 0px;">
                  <label for="inputEmail3" class="col-sm-12 control-label"><?=$jwb->pertanyaan?></label>
                  <div class="col-sm-12">
                    <a target="BLANK" href="<?=base_url('file/'.$jwb->jawaban)?>">Lihat File <?=$jwb->jawaban?></a>
                  </div>
                </div>


                  <!-- <div class="form-group" style="padding: 30px 0px 30px 0px;">
                  <label for="inputEmail3" class="col-sm-12 control-label"><?=$jwb->pertanyaan?></label>
                  <div class="col-sm-12">
                  <img  style="padding: 0px 0px 30px 0px;" width="50%" src="<?=base_url('file/'.$jwb->jawaban)?>"></img>
                  
                  </div>
                  </div> -->


                <?php } ?>

              <?php endif; ?>

              <?php if($jwb->tipe_jawaban == 3): ?>

              <div class="form-group" style="padding: 30px 0px 30px 0px;">
                  <label for="inputEmail3" class="col-sm-12 control-label"><?=$jwb->pertanyaan?></label>
                  <div class="col-sm-12">
                  <?php
                  $xyz = str_replace(',',', ',preg_replace("/[^a-zA-Z, ]/","", $jwb->jawaban));
                  // echo $xyz;
                  ?>
                  <textarea disabled class="form-control"><?=$xyz?></textarea>
                     <!-- <input type="text" disabled class="form-control" required="required" value="<?=$xyz?>">  -->
                  </div>
                </div>

              <?php endif; ?>


              <?php endforeach; ?>

                

                

              </div>
              <!-- /.box-body -->
              <!-- <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Tambah</button>
              </div> -->
              <!-- /.box-footer -->
              <?php echo form_close(); ?>

          </div>

<!-- //riwayat pelatihan -->
          <div class="box box-info">
          <div class="box-header with-border">
              <h3 class="box-title">Riwayat Pelatihan</h3>
            </div> 
  
           <table style="width:100%;" id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Pelatihan</th>
                  <th>Tanggal Pendaftaran</th>
                  <th>Tanggal Kelas</th>
                  <th>Status</th>
                 
                
            
                  
                </tr>
                </thead>
                <tbody>
                <?php 
                $no = 0;
                foreach($data_riwayat_Pelatihan_pendaftar as $all): 
                $no++;
                ?>
               
                  <td><?=$no?></td>
                  <td><?=$all->nama_pelatihan?></td>
                  <td><?=$this->formatter->getDateMonthFormatUser($all->tgl_pembukaan_pendaftaran)?> s/d <?=$this->formatter->getDateMonthFormatUser($all->tgl_penutupan_pendaftaran)?></td>
                  <td><?=$this->formatter->getDateMonthFormatUser($all->tgl_kelas_mulai)?>  s/d <?=$this->formatter->getDateMonthFormatUser($all->tgl_kelas_selesai)?></td>                  
                  <td>
                  <?=status_pendaftaran($all->status)?>
                  </td>    
                   
            
                </tr> 
                <?php endforeach;?>
               
              </tbody>
              </table> 
        </div> 



         <!-- //form quesioner -->
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Update Status Pendaftaran</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php 
            $attributes = array('target' => '_blank');
            echo form_open_multipart('kelolapelatihan/update_status_pendaftaran', $attributes);  ?>
              <div class="box-body">

              <input type="hidden" name="id_pendaftaran"  class="form-control" required="required" value="<?=$data_pendaftaran->id?>"> 
              <input type="hidden" name="id_pendaftar"  class="form-control" required="required" value="<?=$data_pendaftaran->id_user?>">

                  <div class="form-group" style="padding: 0px 0px 30px 0px;">
                    <label class="col-sm-2 control-label">Status Pendaftaran</label>
                    <div class="col-sm-2"> 
                      <select  class="form-control" name="status">
                      <?php foreach($status_pendaftaran as $sp): 
                      $sel = NULL;
                      if($sp->kode == $data_pendaftaran->status){
                        $sel = 'selected';
                      }else{
                        $sel = '';
                      }
                        
                      ?>
                      <option <?=$sel?>  value="<?=$sp->kode?>"><?=$sp->nama?></option>
                      <?php endforeach; ?>
                      </select>
                      <!-- <input type="text" disabled  class="form-control" required="required" value="<?=$jwb->jawaban?>"> -->
                    </div>

                    <div class="col-sm-4">
                    <input type="text" name="alasan"  class="form-control" required="required" value="<?=$data_pendaftaran->keterangan?>" placeholder="Keterangan"> 
                    </div>


                    <div class="col-sm-2">
                    <button type="submit" class="btn btn-info pull-left">Update Status</button>
                    </div>
                  </div>

              </div>
              <!-- /.box-body -->
              <!-- <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Tambah</button>
              </div> -->
              <!-- /.box-footer -->
              <?php echo form_close(); ?>

          </div>





    </div>
    </section>
    <!-- /.content -->

        


  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>
