 <style>
 .linkbutton{
   margin: 3px 3px 3px 3px;
 }
 </style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Kelola Pelatihan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12 col-xs-12">

        <div class="box">
        <div class="box-header">
         <a href="<?=base_url()?>kelolapelatihan/tambah"><button type="button" class="btn btn-sm btn-primary">Tambah Data</button></a> 
         <!--<a onclick="return confirm('Anda Ingin Non Aktifkan Semua User ?')" href="<?=base_url()?>kelolauser/close_masal"><button type="button" class="btn btn-sm btn-danger">Non Aktifkan Massal</button></a> -->
         <!--<a onclick="return confirm('Anda Ingin Aktifkan Semua User ?')" href="<?=base_url()?>kelolauser/open_masal"><button type="button" class="btn btn-sm btn-success">Aktifkan Massal</button></a> -->
        </div>
        
            <!-- /.box-header -->
            <div class="box-body">
              <table style="width:100%" id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Status</th>
                  <th>Kuota Kelas</th>
                  <th>Kuota Pendaftaran</th>
                  <th>Tanggal Pendaftaran</th>
                  <th>Tanggal Kelas</th>
                  
                
                
                 
                  <th width="150px" >Aksi</th>
                  
                </tr>
                </thead>
                <tbody>
                <?php 
                $no = 0;
                foreach($alldata as $all): 
                $no++;
                ?>
               
                  <td><?=$no?></td>
                  <td><?=$all->nama_pelatihan?> | <?=$all->id?></td>
                  <td>
                  
                  <?php if($all->status == 1){ ?>
                  <a href="<?=base_url()?>kelolapelatihan/hidden_pelatihan/<?=$all->id?>/<?=$this->uri->segment(3)?>"><button id="hidden<?=$all->id?>" data-toggle="tooltip" data-placement="left" title="Hidden" type="button" class="btn btn-sm btn-success"><i class="fa fa-eye" style="font-size:12px"></i> Show</button></a>           
                  <?php }else{ ?>

                    <a href="<?=base_url()?>kelolapelatihan/tampilkan_pelatihan/<?=$all->id?>/<?=$this->uri->segment(3)?>"><button id="tampil<?=$all->id?>" data-toggle="tooltip" data-placement="left" title="Tampilkan" type="button" class="btn btn-sm btn-danger"><i class="fa fa-eye-slash" style="font-size:12px"></i> Hidden</button></a>           
                  
                  <?php } ?>
                  
                  
                  </td>

                  <td><?=$all->kuota_kelas?></td>
                  <td><?=$all->kuota_pendaftaran?></td>
                  <td><?=$all->tgl_pembukaan_pendaftaran?> - <?=$all->tgl_penutupan_pendaftaran?></td>
                  <td><?=$all->tgl_kelas_mulai?> - <?=$all->tgl_kelas_selesai?></td>
                  
                 
                  
                  <td>
                  <?php if(cek_quesioner($all->id) < 1){ ?>
                    <a href="<?=base_url()?>kelolapelatihan/quesioner_add/<?=$all->id?>"><button id="quesioner<?=$all->id?>" data-toggle="tooltip" data-placement="left" title="Quesioner" type="button" class="btn btn-sm btn-info linkbutton"><i class="fa fa-quora" style="font-size:12px"></i></button></a>
                  <?php }else{ ?>
                    <a href="<?=base_url()?>kelolapelatihan/quesioner/<?=$all->id?>"><button id="quesioner<?=$all->id?>" data-toggle="tooltip" data-placement="left" title="Quesioner" type="button" class="btn btn-sm btn-info linkbutton"><i class="fa fa-quora" style="font-size:12px"></i></button></a>
                    <?php } ?>

                  <a href="<?=base_url()?>kelolapelatihan/persyaratan/<?=$all->id?>"><button id="persyaratan<?=$all->id?>" data-toggle="tooltip" data-placement="left" title="Persyaratan" type="button" class="btn btn-sm bg-purple linkbutton"><i class="fa fa-bank" style="font-size:12px"></i></button></a>
                  <a href="<?=base_url()?>kelolapelatihan/pendaftar/<?=$all->id?>"><button id="pendaftar<?=$all->id?>" data-toggle="tooltip" data-placement="left" title="Pendaftar" type="button" class="btn btn-sm btn-success  linkbutton"><i class="fa fa-users" style="font-size:12px"></i></button></a>
                  <a href="<?=base_url()?>kelolapelatihan/cetak_pendaftar/<?=$all->id?>"><button id="cetak_pendaftar<?=$all->id?>" data-toggle="tooltip" data-placement="left" title="Cetak List Pendaftar" type="button" class="btn btn-sm bg-maroon  linkbutton"><i class="fa fa-print" style="font-size:12px"></i></button></a>
                  <a href="<?=base_url()?>kelolapelatihan/testimoni/<?=$all->id?>"><button id="testi<?=$all->id?>" data-toggle="tooltip" data-placement="left" title="Testimoni Alumni" type="button" class="btn btn-sm bg-navy linkbutton"><i class="fa fa-comments" style="font-size:12px"></i></button></a>
                  <a href="<?=base_url()?>kelolapelatihan/edit/<?=$all->id?>"><button id="edit<?=$all->id?>" data-toggle="tooltip" data-placement="left" title="Edit" type="button" class="btn btn-sm btn-warning  linkbutton"><i class="fa fa-pencil" style="font-size:12px"></i></button></a>           
                  <a href="<?=base_url()?>kelolapelatihan/hapus/<?=$all->id?>"><button id="hapus<?=$all->id?>" data-toggle="tooltip" data-placement="left" title="Hapus" onclick="return confirm('Anda ingin menghapus ?')" type="button" class="btn btn-sm btn-danger linkbutton "><i class="fa fa-trash" style="font-size:12px"></i></button></a>
                  </td>

                  
            
                </tr> 
                <?php endforeach;?>
               
              </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->



        </div>
    </div>
    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>
  <?php 
  $no = 0;
  foreach($alldata as $all): 
  $no++;
  ?>
                <script>
                  $(document).ready(function(){ 
                    $('#quesioner<?=$all->id?>').tooltip();
                    $('#pendaftar<?=$all->id?>').tooltip();
                    $('#persyaratan<?=$all->id?>').tooltip();
                    $('#edit<?=$all->id?>').tooltip();
                    $('#hapus<?=$all->id?>').tooltip();
                    $('#testi<?=$all->id?>').tooltip();
                    $('#hidden<?=$all->id?>').tooltip();
                    $('#tampil<?=$all->id?>').tooltip();
                  });
                  </script>
  <?php endforeach;?>

 


  <script>
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true,
      "fixedHeader": true
    });

</script>