  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Kelola Peserta Pelatihan <?=$detail->nama_pelatihan?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12 col-xs-12">

        <div class="box">
        <div class="box-header">
        <?php if($detail->form_testimoni == 1){ ?>
                  <a href="<?=base_url()?>kelolapelatihan/hidden_tombol_testimoni/<?=$detail->id?>"><button id="form<?=$detail->id?>" data-toggle="tooltip" data-placement="left" title="Hidden Tombol Testimoni" type="button" class="btn btn-sm btn-success"><i class="fa fa-eye" style="font-size:12px"></i> Show Tombol Testimoni</button></a>           
                  <?php }else{ ?>

                    <a href="<?=base_url()?>kelolapelatihan/tampilkan_tombol_testimoni/<?=$detail->id?>"><button id="form<?=$detail->id?>" data-toggle="tooltip" data-placement="left" title="Tampilkan Tombol Testimoni" type="button" class="btn btn-sm btn-danger"><i class="fa fa-eye-slash" style="font-size:12px"></i> Hidden Tombol Testimoni</button></a>           
                   
                  <?php } ?>
         <!--<a onclick="return confirm('Anda Ingin Non Aktifkan Semua User ?')" href="<?=base_url()?>kelolauser/close_masal"><button type="button" class="btn btn-sm btn-danger">Non Aktifkan Massal</button></a> -->
         <!--<a onclick="return confirm('Anda Ingin Aktifkan Semua User ?')" href="<?=base_url()?>kelolauser/open_masal"><button type="button" class="btn btn-sm btn-success">Aktifkan Massal</button></a> -->
        </div>
        
            <!-- /.box-header -->
            <div class="box-body">
              <table style="width:100%" id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="5%">No</th>
                  <th width="15%">Nama</th>
                  <th width="15%">NIK</th>
                  <th>Testimoni</th>
                 
                
                
                 
                  <th width="5%" >Status</th>
                  
                </tr>
                </thead>
                <tbody>
                <?php 
                $no = 0;
                foreach($alldata as $all): 
                $no++;
                ?>
               
                  <td><?=$no?></td>
                  <td><?=$all->nama?></td>
                  <td><?=$all->nik?></td>
                  <td><?=$all->testimoni?></td>
                 
                 
                  
                  <td >
                  <?php if($all->status_testimoni == 1){ ?>
                  <a href="<?=base_url()?>kelolapelatihan/hidden_testimoni/<?=$all->id?>/<?=$this->uri->segment(3)?>"><button id="hidden<?=$all->id?>" data-toggle="tooltip" data-placement="left" title="Hidden" type="button" class="btn btn-sm btn-success"><i class="fa fa-eye" style="font-size:12px"></i> Show</button></a>           
                  <?php }else{ ?>

                    <a href="<?=base_url()?>kelolapelatihan/tampilkan_testimoni/<?=$all->id?>/<?=$this->uri->segment(3)?>"><button id="tampil<?=$all->id?>" data-toggle="tooltip" data-placement="left" title="Tampilkan" type="button" class="btn btn-sm btn-danger"><i class="fa fa-eye-slash" style="font-size:12px"></i> Hidden</button></a>           
                  
                  <?php } ?>
                  
                </tr> 
                <?php endforeach;?>
               
              </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->



        </div>
    </div>
    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>
  <?php 
  $no = 0;
  foreach($alldata as $all): 
  $no++;
  ?>
                <script>
                  $(document).ready(function(){ 
                    $('#hidden<?=$all->id?>').tooltip();
                    $('#tampil<?=$all->id?>').tooltip();
                  });
                  </script>
  <?php endforeach;?>

 


  <script>
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            // 'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true,
      "fixedHeader": true
    });

</script>