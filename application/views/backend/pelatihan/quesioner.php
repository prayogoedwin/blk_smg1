  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Quesioner Pelatihan <?=$detail->nama_pelatihan?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Tambah Quesioner Pelatihan <?=$detail->nama_pelatihan?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      

    <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('kelolapelatihan/aksi_update_quesioner');  ?>
              <div class="box-body">
              

              <input type="hidden" name="id_pelatihan" value="<?=$this->uri->segment(3)?>">
              <table style="width:100%" id="" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Tipe</th>
                  <th>Pertanyaan</th>
                  
                  <th>
                  <!-- <input type="checkbox" onclick="toggle(this);" /> -->
                  Option
                  </th>
                  
                </tr>
                </thead>
                <tbody>
                <?php 
                $no = 0;
                foreach($quesioner as $all): 
                $no++;
                ?>
               
                  <td><?=$no?></td>
                  <td> <?=$all->tipe?></td>
                  <td>
                  <input type="hidden" name="id_pelatihan_quesioner[]" value="<?=$all->id_pelatihan_quesioner?>">
                  <?=$all->pertanyaan?></td>

                  
                 
                  
                  <td>

                  

                  <!-- <div class="checkbox">
                  <label>
                  <input <?php echo $all->quesioner == 1 ? 'checked' : ''; ?> type="checkbox" name="quesioner[]"  id="<?=$all->id_pelatihan_quesioner?>"  value="0">
                  </label>
                  </div> 
                   -->

                  
                  <?php if($all->quesioner == 1){
                    $col = 'green';
                  }else{
                    $col = 'red';
                  }
                  ?>

                  <select style="background-color:<?=$col?>; color:#fff" name="quesioner[]" class="form-control" required  style="width:100px;">

                  <option  <?php echo $all->quesioner == 1 ? 'selected' : ''; ?> value="1">Ya</option>
                  <option <?php echo $all->quesioner == 0 ? 'selected' : ''; ?> value="0">Tidak</option>
                  
                  </select> 

                  </td>
                 
                  
            
                </tr> 
                <?php endforeach;?>
               
              </tbody>
              </table>
            
              
               
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Update</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>


  <?php include(__DIR__ . "/../template/footer.php"); ?>

  <script>
  function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}



