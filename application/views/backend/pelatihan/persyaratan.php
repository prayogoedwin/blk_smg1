  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Kelola Persyaratan Pelatihan <?=$detail->nama_pelatihan?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-6">

        <div class="box">
        <div class="box-header">
         <!-- <a href="<?=base_url()?>kelolapelatihan/tambah_persyaratan"><button type="button" class="btn btn-sm btn-primary">Tambah Data</button></a>  -->
         <!--<a onclick="return confirm('Anda Ingin Non Aktifkan Semua User ?')" href="<?=base_url()?>kelolauser/close_masal"><button type="button" class="btn btn-sm btn-danger">Non Aktifkan Massal</button></a> -->
         <!--<a onclick="return confirm('Anda Ingin Aktifkan Semua User ?')" href="<?=base_url()?>kelolauser/open_masal"><button type="button" class="btn btn-sm btn-success">Aktifkan Massal</button></a> -->
        </div>
        
            <!-- /.box-header -->
            <div class="box-body">
              <table style="width:100%" id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="10px">No</th>
                  <th>Persyaratan</th>
                  <th width="150px" >Aksi</th>
                  
                </tr>
                </thead>
                <tbody>
                <?php 
                $no = 0;
                foreach($alldata as $all): 
                $no++;
                ?>
               
                  <td><?=$no?></td>
                  <td><?=$all->deskripsi?></td>
                 
                 
                  
                  <td >

                  <a class="btn btn-danger" onclick="return confirm('Anda ingin menghapus ?')" href="<?=base_url()?>kelolapelatihan/hapus_persyaratan/<?=$all->id?>/<?=$detail->id?>"><i class="fa fa-trash" style="font-size:12px"></i></a></td>
                

                  
            
                </tr> 
                <?php endforeach;?>
               
              </tbody>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->



        </div>

        <div class="col-lg-6">
        <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('kelolapelatihan/aksi_tambah_persyaratan');  ?>
              <div class="box-body">

              <input type="hidden" name="id_pelatihan" class="form-control" value="<?=$detail->id?>" required="required">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Persyaratan</label>
                  <div class="col-sm-10">
                  <select name="persyaratan[]" class="form-control select2" multiple="multiple" required data-placeholder="&nbsp;&nbsp;Pilih Persyaratan" style="width: 100%;">
                  <?php foreach($persyaratan as $syarat): ?>
                  <option value="<?=$syarat->id?>"><?=$syarat->deskripsi?></option>
                  <?php endforeach; ?>
                 
                </select>
                  </div>
                </div>

                <!-- <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Persyaratan</label>
                  <div class="col-sm-8">
                  <?php foreach($persyaratan as $syarat): ?>
                  <label>
                  <input type="checkbox" class="minimal" >
                  Minimal skin checkbox
                  </label>
                  <option value="<?=$syarat->id?>"><?=$syarat->deskripsi?></option>
                  <?php endforeach; ?>
                  
                 
                
                  </div>
                </div> -->

               

                
               
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Tambah</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>
    </div>
    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>
  <?php 
  $no = 0;
  foreach($alldata as $all): 
  $no++;
  ?>
                <script>
                  $(document).ready(function(){ 
                    $('#quesioner<?=$all->id?>').tooltip();
                    $('#pendaftar<?=$all->id?>').tooltip();
                    $('#edit<?=$all->id?>').tooltip();
                    $('#hapus<?=$all->id?>').tooltip();
                  });
                  </script>
  <?php endforeach;?>

 


  <script>
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true,
      "fixedHeader": true
    });

</script>