  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Tambah Pelatihan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Tambah Pelatihan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      

    <div class="col-md-10">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open('kelolapelatihan/aksi_tambah');  ?>
              <div class="box-body">

              <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Nama Pelatihan</label>
                  <div class="col-sm-8">
                    <input type="text" name="nama_pelatihan" class="form-control"  placeholder="Nama Pelatihan" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Kuota Pendaftaran</label>
                  <div class="col-sm-8">
                    <input type="text" name="kuota_pendaftaran" class="form-control"   placeholder="Kuota Pendaftaran" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Kuota Kelas </label>
                  <div class="col-sm-8">
                    <input type="text" name="kuota_kelas" class="form-control"   placeholder="Kuota Kelas" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Tanggal Pembukaan Pendaftaran</label>
                  <div class="col-sm-8">
                    <input type="date" name="tgl_buka_p" class="form-control"    required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Tanggal Penutupan Pendaftaran</label>
                  <div class="col-sm-8">
                    <input type="date" name="tgl_tutup_p" class="form-control"  required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Tanggal Mulai Kelas</label>
                  <div class="col-sm-8">
                    <input type="date" name="tgl_mulai_k" class="form-control"  required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Tanggal Selesai Kelas</label>
                  <div class="col-sm-8">
                    <input type="date" name="tgl_selesai_k" class="form-control"  required="required">
                  </div>
                </div>
               

                <div class="form-group" >
                  <label for="inputEmail3" class="col-sm-4 control-label">Deskripsi Pelatihan</label>
                  <div class="col-sm-8">
                    <textarea name="deskripsi" class="form-control" style="width:100%; height:100px"></textarea>
                  </div>
                </div>
                

                <div class="form-group" hidden>
                  <label for="inputEmail3" class="col-sm-4 control-label">Persyaratan</label>
                  <div class="col-sm-8">
                  <select name="persyaratan[]" class="form-control select2" multiple="multiple" required data-placeholder="&nbsp;&nbsp;Pilih Persyaratan" style="width: 100%;">
                  <?php foreach($persyaratan as $syarat): ?>
                  <option <?=$syarat->as?> value="<?=$syarat->id?>"><?=$syarat->deskripsi?></option>
                  <?php endforeach; ?>
                 
                </select>
                  </div>
                </div>

                <!-- <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Persyaratan</label>
                  <div class="col-sm-8">
                  <?php foreach($persyaratan as $syarat): ?>
                  <label>
                  <input type="checkbox" class="minimal" >
                  Minimal skin checkbox
                  </label>
                  <option value="<?=$syarat->id?>"><?=$syarat->deskripsi?></option>
                  <?php endforeach; ?>
                  
                 
                
                  </div>
                </div> -->

               

                
               
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Tambah</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>