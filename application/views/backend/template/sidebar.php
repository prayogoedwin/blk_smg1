

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
        <img src="<?=base_url('assets/user.png')?>" class="img-circle" alt="User Image"> 
        </div>
        <div class="pull-left info">
        <p><?= get_user($this->session->userdata('id_pengguna'))->nama; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU KONTEN</li>
        <li><a href="<?=base_url()?>dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <li><a href="<?=base_url()?>kelolauser/admin"><i class="fa fa-lock"></i>  <span>Akun Admin</span></a></li>
        <li><a href="<?=base_url()?>kelolaslider"><i class="fa fa-image"></i> <span>Slider</span></a></li>
        <li><a href="<?=base_url()?>kelolafaq"><i class="fa fa-question-circle"></i> <span>FAQ</span></a></li>
        <li><a href="<?=base_url()?>kelolapesan"><i class="fa fa-envelope"></i> <span>Pesan</span></a></li>
        <!-- <li><a href="<?=base_url()?>kelolakontak"><i class="fa fa-phone"></i> <span>Kontak</span></a></li> -->
        <br/>

        <li class="header">MENU PELATIHAN</li>
        <li><a href="<?=base_url()?>kelolauser"><i class="fa fa-users"></i> <span>Akun Peserta</span></a></li>
        <li><a href="<?=base_url()?>kelolapersyaratan"><i class="fa fa-legal"></i> <span>Persyaratan</span></a></li>
        <li><a href="<?=base_url()?>kelolapelatihan"><i class="fa fa-ticket"></i> <span>Pelatihan</span></a></li>
        <!-- <li><a href="<?=base_url()?>kelolaalumni"><i class="fa fa-graduation-cap"></i> <span>Alumin</span></a></li> -->


 
        <!-- <li><a href="<?=base_url()?>monitoring"><i class="fa fa-desktop"></i> <span>Monitoring</span></a></li> -->

        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-sitemap"></i> <span>Masterdata</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()?>spm"><i class="fa fa-book"></i> <span>Jenis SPM</span></a></li>
            <li><a href="<?=base_url()?>pelayanan"><i class="fa fa-book"></i> <span>Jenis Pelayanan Dasar</span></a></li>
            <li><a href="<?=base_url()?>informasi"><i class="fa fa-book"></i> <span>Informasi</span></a></li>
          </ul>
        </li> -->

       
       
       
        
        
         
        
      </ul>
    </section>
  </aside>