  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Tambah User</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Tambah User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      

    <div class="col-md-8">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('kelolauser/aksi_tambah');  ?>
              <div class="box-body">
              

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Kab/Kota</label>

                  <div class="col-sm-10">
                  <select class="form-control select2" name="kota" style="width: 100%;">
                  <option>Pilih Kab/Kota User</option>
                    <?php
                     
                      foreach($kota as $b){
                          echo "<option value='".$b->id_kota."'>".$b->nama_kota."</option>";
                      }
                    ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">OPD</label>

                  <div class="col-sm-10">
                  <select class="form-control select2" name="skpd" style="width: 100%;">
                  <option>Pilih OPD User</option>
                    <?php
                     
                      foreach($skpd as $c){
                          echo "<option value='".$c->kd."'>".$c->nm."</option>";
                      }
                    ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Pengampu</label>

                  <div class="col-sm-10">
                  <select class="form-control select2" name="pengampu" style="width: 100%;">
                  <option>Pilih SPM User</option>
                    <?php
                     
                      foreach($pengampu as $d){
                          echo "<option value='".$d->id_jenis_spm."'>".$d->nama_jenis_spm."</option>";
                      }
                    ?>
                    </select>
                  </div>
                </div>





                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input type="username" name="nama" class="form-control" id="inputEmail3" placeholder="Nama Admin" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <input type="username" required="required" name="username" class="form-control" id="inputEmail3" placeholder="Usermame">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                    <input type="password" required="required" name="password" class="form-control" id="inputPassword3" placeholder="Password">
                  </div>
                </div>
                

               
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Tambah</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>