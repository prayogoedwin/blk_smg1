  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Edit Password</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Edit Password</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      

    <div class="col-md-9">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('dashboard/aksi_edit_password');  ?>
              <div class="box-body">
              <div class="form-group">
                  <label for="inputPassword3" class="col-sm-3 control-label">Akses</label>

                  <div class="col-sm-9">
                  <select class="form-control select2" name="akses" style="width: 100%;" disabled>
                  <option value="<?=$detail->id_akses?>"><?=$detail->akses?></option>
                    <?php
                     
                      foreach($akses as $a){
                          echo "<option value='".$a->id_akses."'>".$a->nama_akses."</option>";
                      }
                    ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-3 control-label">Kab/Kota</label>

                  <div class="col-sm-9">
                  <select class="form-control select2" name="kota" style="width: 100%;" disabled> 
                  <option value="<?=$detail->id_kota?>"><?=$detail->kabkota?></option>
                    <?php
                     
                      foreach($kota as $b){
                          echo "<option value='".$b->id_kota."'>".$b->nama_kota."</option>";
                      }
                    ?>
                    </select>
                  </div>
                </div>

                <div class="form-group" hidden>
                  <label for="inputPassword3" class="col-sm-3 control-label">OPD</label>

                  <div class="col-sm-9">
                  <select class="form-control select2" name="skpd" style="width: 100%;" disabled>
                  <option value="<?=$detail->id_skpd?>"><?=$detail->skpd?></option>
                    <?php
                     
                      foreach($skpd as $c){
                          echo "<option value='".$c->kd."'>".$c->nm."</option>";
                      }
                    ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-3 control-label">Pengampu SPM</label>

                  <div class="col-sm-9">
                  <select class="form-control select2" name="pengampu" style="width: 100%;" disabled>
                  <option value="<?=$detail->id_pengampu_spm?>"><?=$detail->pengampu_spm?></option>
                    <?php
                     
                      foreach($pengampu as $d){
                          echo "<option value='".$d->id_jenis_spm."'>".$d->nama_jenis_spm."</option>";
                      }
                    ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Nama</label>
                  <div class="col-sm-9">
                    <input type="hidden" name="id_user" class="form-control" id="inputEmail3" value="<?=$detail->id_user?>" placeholder="Nama Admin" required="required">
                    <input type="username" disabled name="nama" class="form-control" id="inputEmail3" value="<?=$detail->nama?>" placeholder="Nama Admin" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Username</label>
                  <div class="col-sm-9">
                    <input type="username" disabled required="required" name="username" value="<?=$detail->nip?>" class="form-control" id="inputEmail3" placeholder="Usermame">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Password</label>
                  <div class="col-sm-9">
                    <input type="password"  required="required" name="password"  class="form-control" id="inputEmail3" placeholder="***************">
                  </div>
                </div>
               
                

               
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Edit</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>