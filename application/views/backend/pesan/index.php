  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Kelola Pesan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12 col-xs-12">

        <div class="box">
        <div class="box-header">
         <a href="<?=base_url()?>kelolapesan/tambah"><button type="button" class="btn btn-sm btn-primary">Tambah Data</button></a> 
         <!--<a onclick="return confirm('Anda Ingin Non Aktifkan Semua User ?')" href="<?=base_url()?>kelolapesan/close_masal"><button type="button" class="btn btn-sm btn-danger">Non Aktifkan Massal</button></a> -->
         <!--<a onclick="return confirm('Anda Ingin Aktifkan Semua User ?')" href="<?=base_url()?>kelolapesan/open_masal"><button type="button" class="btn btn-sm btn-success">Aktifkan Massal</button></a> -->
        </div>
        
            <!-- /.box-header -->
            <div class="box-body">
              <table style="width:100%" id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Pengirim</th>
                  <th>Kontak</th>
                  <th>Judul</th>
                  <th>Isi</th>
                  <!-- <th width="100px">Aksi</th> -->
                  
                </tr>
                </thead>
                <tbody>
                <?php 
                $no = 0;
                foreach($alldata as $all): 
                $no++;
                ?>
               
                  <td><?=$no?></td>
                  <td><?=$all->name?></td>
                  <td>Email: <?=$all->email?><br/> 
                  HP: <?=$all->hp?><br/>
                  WA: <?=$all->wa?></td>
                  <td><?=$all->subject?></td>
                  <td><?=$all->message?></td>
                  
                  <!-- <td>
                
                  <a href="<?=base_url()?>kelolapesan/edit/<?=$all->id?>"><button type="button" class="btn btn-sm btn-warning"><i class="fa fa-pencil" style="font-size:12px"></i></button></a>
             
                  <a href="<?=base_url()?>kelolapesan/hapus/<?=$all->id?>"><button onclick="return confirm('Anda ingin menghapus ?')" type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash" style="font-size:12px"></i></button></a></td> -->
            
                </tr> 
                <?php endforeach;?>
               
              </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->



        </div>
    </div>
    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>


  <script>
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "scrollX": true,
      "fixedHeader": true
    });

</script>