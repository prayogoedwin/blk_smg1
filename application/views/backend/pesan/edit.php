  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Edit FAQ</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Edit FAQ</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      

    <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open_multipart('kelolafaq/aksi_edit');  ?>
              <div class="box-body">
              





                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Pertanyaan</label>
                  <div class="col-sm-10">
                    <input type="hidden" name="id" class="form-control" id="inputEmail3" value="<?=$detail->id?>" placeholder="Nama Admin" required="required">
                    <textarea class="form-control" required="required" name="pertanyaan"><?=$detail->pertanyaan?></textarea>
                    <!-- <input type="text" name="pertanyaan" class="form-control" id="inputEmail3" value="<?=$detail->pertanyaan?>" placeholder="Pertanyaan" required="required"> -->
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Jawaban</label>
                  <div class="col-sm-10">
                  <textarea class="form-control" required="required" name="jawaban"><?=$detail->jawaban?></textarea>
                    <!-- <input type="text" required="required" name="jawaban" value="<?=$detail->jawaban?>" class="form-control" id="inputEmail3" placeholder="Jawaban"> -->
                  </div>
                </div>
               
                

               
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Edit</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>