  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Tambah FAQ</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Tambah FAQ</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      

    <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open('kelolafaq/aksi_tambah');  ?>
              <div class="box-body">
              

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Pertanyaan</label>
                  <div class="col-sm-10">
                  <textarea class="form-control" required="required" name="pertanyaan"></textarea>
                    <!-- <input type="text" name="pertanyaan" class="form-control" id="inputEmail3" placeholder="Pertanyaan" required="required"> -->
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Jawaban</label>
                  <div class="col-sm-10">
                  <textarea class="form-control" required="required" name="jawaban"></textarea>
                    <!-- <input type="text" name="jawaban" class="form-control" id="inputEmail3" placeholder="Jawaban" required="required"> -->
                  </div>
                </div>
               
                

               
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Tambah</button>
              </div>
              <!-- /.box-footer -->
              <?php echo form_close(); ?>
          </div>
        </div>

    
      

    </section>
    <!-- /.content -->
  </div>

  <?php include(__DIR__ . "/../template/footer.php"); ?>