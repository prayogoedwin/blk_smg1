 <!-- ======= Contact Section ======= -->
 <section id="kontak" class="contact">
      <div class="container" data-aos="fade-up">
          <br/>
          <br/>
          <br/>

        <div class="section-title">
          <h2>Kirim Testimoni Anda Untuk Pelatihan <?=$pelatihan->nama_pelatihan?></h2>
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>

       
        <div class="row">


          <div class="col-lg-12 mt-5 mt-lg-0">

            <!-- <form action="forms/contact.php" method="post" role="form" class="php-email-form"> -->
            <?php 
             $info = $this->session->flashdata('info');
             $pesan = $this->session->flashdata('message');
            echo form_open_multipart('publik/aksi_kirim_testimoni', 'class=""');   
                                  
            if( $info == 'danger'){ ?>
                
                <span style="color:red"><?=$pesan?> </span>
                
            <?php } ?>

            <?php if( $info == 'success'){ ?>
                                      
              <span style="color:green"> <?=$pesan?> </span>
              
          <?php } ?>

              <div class="form-row">
                <div class="col-md-12 form-group">
                Deskripsi
                <textarea disabled class="form-control" rows="5"><?=$pelatihan->deskripsi?></textarea>
                  <!-- <input type="text" name="kuota" class="form-control" id="name" placeholder="Kuota Kelas" data-rule="required:true"  value="<?=$pelatihan->kuota_kelas?>"/> -->
                  <div class="validate"></div>
                </div>
                
                
              </div>

              <input type="hidden" class="form-control" name="id_pelatihan"  value="<?=$pelatihan->id?>">

              <div class="form-row">
              <div class="col-md-6 form-group">
                    Tanggal Pembukaan Pendaftaran
                  <input type="date" disabled class="form-control" name="t_p_p"  value="<?=$pelatihan->tgl_pembukaan_pendaftaran?>"/>
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                    Tanggal Penutupan Pendaftaran
                  <input type="date" disabled class="form-control" name="t_p_p"  value="<?=$pelatihan->tgl_penutupan_pendaftaran?>"/>
                  <div class="validate"></div>
                </div>
              </div>

              <div class="form-row">
              <div class="col-md-6 form-group">
                    Tanggal Kelas Mulai
                  <input type="date" disabled class="form-control" name="t_p_p"  value="<?=$pelatihan->tgl_kelas_mulai?>"/>
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                    Tanggal Kelas Berakhir
                  <input type="date" disabled class="form-control" name="t_p_p"  value="<?=$pelatihan->tgl_kelas_selesai?>"/>
                  <div class="validate"></div>
                </div>
              </div>
              <input type="hidden" class="form-control" name="id_pendaftaran"  value="<?=$this->uri->segment(3)?>"/>
              <div class="form-row">
              <div class="col-md-12 form-group">
                    Testimoni Anda
                  <textarea class="form-control" name="testimoni"><?=$pendaftaran->testimoni?></textarea>
                  
                  <div class="validate"></div>
                </div>
                
              </div>

              <input type="hidden" class="form-control" name="id_pendaftaran"  value="<?=$this->uri->segment(3)?>"/>
              <div class="form-row">
              <div class="col-md-12 form-group">
                    Link Video Testimoni (Youtube)
                  <textarea class="form-control" name="video"><?=$pendaftaran->video?></textarea>
                  
                  <div class="validate"></div>
                </div>
                
              </div>

            
              
              

              
              
              <div class="text-center"><button class="btn btn-success" type="submit">Kirim Testimoni</button></div>
              <?php echo form_close(); ?>

          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

    <?php
  $this->load->view('frontend/a_footer_acc');
  ?>