 <!-- ======= Contact Section ======= -->
 <section id="kontak" class="contact">
      <div class="container" data-aos="fade-up">
          <br/>
          <br/>
          <br/>

        <div class="section-title">
          <h2>Riwayat Pelatihan Anda</h2>
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>

       
        <div class="row">


          <div class="col-lg-12 mt-5 mt-lg-0">

            <!-- <form action="forms/contact.php" method="post" role="form" class="php-email-form"> -->
            <?php 
             $info = $this->session->flashdata('info');
             $pesan = $this->session->flashdata('message');
            echo form_open('publik/aksi_register', 'class=""');   
                                  
            if( $info == 'danger'){ ?>
                
                <span style="color:red"><?=$pesan?> </span>
                
            <?php } ?>

            <?php if( $info == 'success'){ ?>
                                      
              <span style="color:green"><?=$pesan?> </span>
              
          <?php } ?>

              
          <table style="width:100%; margin-bottom:230px" id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Pelatihan</th>
                  <th>Tanggal Pendaftaran</th>
                  <th>Tanggal Kelas</th>
                  <th>Status</th>
                  <th>Keterangan</th>
                 
                
            
                  
                </tr>
                </thead>
                <tbody>
                <?php 
                $no = 0;
                foreach($alldata as $all): 
                $no++;
                ?>
               
                  <td><?=$no?></td>
                  <td><?=$all->nama_pelatihan?>
                  
                  <?php if($all->form_testimoni == 1 && $all->status == 2){ ?>
                  <a href="<?=base_url()?>publik/kirim_testimoni/<?=$all->id?>/<?=$all->id_pelatihan?>"><button id="testi<?=$all->id?>" data-toggle="tooltip" data-placement="left" title="Kirim testimoni" type="button" class="btn btn-sm btn-success"><i class="fa fa-comments" style="font-size:12px"></i> Kirim Testimoni</button></a>           
                  <?php }
                  ?>

                  </td>
                  <td><?=$this->formatter->getDateMonthFormatUser($all->tgl_pembukaan_pendaftaran)?> s/d <?=$this->formatter->getDateMonthFormatUser($all->tgl_penutupan_pendaftaran)?></td>
                  <td><?=$this->formatter->getDateMonthFormatUser($all->tgl_kelas_mulai)?>  s/d <?=$this->formatter->getDateMonthFormatUser($all->tgl_kelas_selesai)?></td>                  
                  <td>
                  <?php 
                  $x = NULL;
                  if($all->status == 3){
                    $x = 'danger';
                    $y = 'red';
                  }else{
                    $x = 'success';
                    $y = 'green';
                  }
                  ?>

                  <span style="color:<?=$y?>"><?=status_pendaftaran($all->status)?></span>
                  <!-- <a href="<?=base_url()?>publik/detail_pendaftaran/<?=$all->id?>"><button  data-toggle="tooltip" data-placement="left" title="Detail" type="button" class="btn btn-sm btn-<?=$x?>"><?=status_pendaftaran($all->status)?></button></a> -->
                  <!-- <span class="label label-<?=$x?>"><?=status_pendaftaran($all->status)?></span> -->
                  <!-- <button data-toggle="tooltip" data-placement="left" title="Detail" type="button" class="btn btn-sm btn-<?=$x?>"></button> -->
                  </td>    
                  <td><?=$all->keterangan?></td>
                   
            
                </tr> 
                <?php endforeach;?>
               
              </tbody>
              </table>

          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

    <?php
  $this->load->view('frontend/a_footer_acc');
  ?>

<?php 
  $no = 0;
  foreach($alldata as $all): 
  $no++;
  ?>
                <script>
                  $(document).ready(function(){ 
                    $('#testi<?=$all->id?>').tooltip();
                  });
                  </script>
  <?php endforeach;?>