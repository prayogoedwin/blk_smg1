 <!-- ======= Contact Section ======= -->
 <section id="kontak" class="contact">
      <div class="container" data-aos="fade-up">
          <br/>
          <br/>
          <br/>

        <div class="section-title">
          <h2>Login</h2>
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>

       
        <div class="row" style="margin-bottom:250px">


          <div class="col-lg-12 mt-5 mt-lg-0">

            <!-- <form action="forms/contact.php" method="post" role="form" class="php-email-form"> -->
            <?php 
             $info = $this->session->flashdata('info');
             $pesan = $this->session->flashdata('message');
            echo form_open('portal/cek_login_peserta');   
                                  
            if( $info == 'danger'){ ?>
                
                <span style="color:red"><?=$pesan?> </span>
                
            <?php } ?>

            <?php if( $info == 'success'){ ?>
                                      
              <span style="color:green"><?=$pesan?> </span>
              
          <?php } ?>

              <div class="form-row">
                <div class="col-md-5 form-group">
                Email
                  <input type="text" name="user" class="form-control"  placeholder="Email"  />
                  <div class="validate"></div>
                </div>
                <div class="col-md-5 form-group">
                Password
                  <input type="password" name="password" class="form-control"  placeholder="Password"  />
                  <!-- <input type="password" name="password" class="form-control" id="password" placeholder="Password" data-rule="minlen:16" data-msg="Minimal 16 Digit" /> -->
                  <div class="validate"></div>
                </div>

                <div class="col-md-2 form-group">
                &nbsp;
                <div class="text-center"><button type="submit" class="btn btn-success">Login</button></div>
                </div>
                
            </div>
            <p>Belum memiliki akun?&nbsp;Register <a href="<?=base_url('publik/register')?>">disini</a></p>

              <!-- <div class="form-row">
                <div class="col-md-6 form-group">
                    Password
                  <input type="password" name="password" class="form-control" id="password" placeholder="Password" data-rule="minlen:6" data-msg="Minimal 6 Karakter" />
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                    Ulangi Password
                <input type="password" name="re-password" class="form-control" id="re-password" placeholder="Ulangi Password" data-rule="minlen:6" data-msg="Minimal 6 Karakter" />
                  <div class="validate"></div>
                </div>
              </div> -->

              

              
              <!-- <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                <div class="validate"></div>
              </div> -->
              <!-- <div class="mb-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div> -->
              <!-- <div class="text-center"><button type="submit">Registrasi</button></div> -->
            <!-- </form> -->
            <?php echo form_close(); ?>

          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

    <?php
  $this->load->view('frontend/a_footer_acc');
  ?>