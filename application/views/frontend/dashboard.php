<!-- ======= Featured Section ======= -->
<section id="pelatihan" class="featured">
  <div class="container">

  <br/>
  <br/>
  <br/>

  <div class="section-title">
      <?php if($this->session->userdata('id_pengguna') != ''){ ?>
          <h2>Selamat Datang <a href="<?=base_url('publik/profil')?>"><?=$profil->nama?></a> Di Official Website BLK Semarang 1</h2>
      <?php } ?>
         
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>

    <div class="row">
     <?php foreach($pelatihan as $pel): ?>
          <div class="col-lg-4">
            <div class="icon-box">
              <!-- <i class="icofont-learn"></i> -->
              <h3><a href=""><?=$pel->nama_pelatihan?></a></h3>
              <p style="font-size:16px">Pendaftaran :<br/><?=$this->formatter->getDateMonthFormatUser($pel->tgl_pembukaan_pendaftaran)?> - <?=$this->formatter->getDateMonthFormatUser($pel->tgl_penutupan_pendaftaran)?></p>
              <div class="expanded-text">
                <span class="short-name" style="color:red">Lihat Detail</span>
               
                <?php 
           

                $mulai = new DateTime($pel->tgl_kelas_mulai);
                $selesai = new DateTime($pel->tgl_kelas_selesai);

                $jumlah_hari = $mulai->diff($selesai)->format("%d Hari Pelatihan");
        

                ?>
                <span class="longer-name"><?=$jumlah_hari?></span>
                <span class="longer-name">GRATIS</span>
                <span class="longer-name">Syarat:</span>
                <span class="longer-name"><?php foreach(syarat($pel->id) as $per){ echo '<li>'.$per->deskripsi.'</li>'; }; ?> </span>
               <?php $a = $this->session->userdata('id_pengguna');
                if( $a == ''){ ?>
                  <a href="<?=base_url('publik/login')?>" type="button" class="btn" style="background-color:#fff; float: right; border-color:#D00000;">Login Untuk Mendaftar</a>
                <?php }else{ ?> 
                  <?php if(cek_keikutsertaan($a, $pel->id) > 0){ ?>
                      <a href="<?=base_url('publik/riwayat/')?>" type="button" class="btn" style="background-color:#fff; float: right; border-color:#D00000;">Anda Sudah Mendaftar</a>
                    <?php }else{ ?>
                      <a href="<?=base_url('publik/daftar_pelatihan/'.$pel->id)?>" type="button" class="btn" style="background-color:#fff; float: right; border-color:#D00000;">Daftar Pelatihan</a>
                    <?php }?>

                  <!-- <a href="<?=base_url('publik/daftar_pelatihan/'.$pel->id)?>" type="button" class="btn" style="background-color:#fff; float: right; border-color:#D00000;">Daftar Pelatihan</a> -->
                  <?php }?> 
                </div>
                <br/>
                

                
            </div>
            <br/>
          </div>
         
          <?php endforeach; ?>
      
    </div>
    <br/>
   

  </div>
</section><!-- End Featured Section -->

    <?php
  $this->load->view('frontend/a_footer_acc');
  ?>