  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container d-flex">

      <div class="logo mr-auto" style="">
      <a href="<?=base_url()?>">
        <!-- <h1 class="text-light"><a href="index.html"><span>BLK SEMARANG</span></a></h1> -->
        <img src="<?=base_url()?>blk_smg/satu/assets/logo_tes.png" alt=""  style="padding-top:-10">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        </a>
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
         
          

          <!-- <li class="drop-down"><a href="#">About</a>
            <ul>
              <li><a href="about.html">About Us</a></li>
              <li><a href="team.html">Team</a></li>

              <li class="drop-down"><a href="#">Drop Down 2</a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
            </ul>
          </li> -->
          <li class="active"><a href="<?=base_url()?>">Beranda</a></li>
          <?php 
          $a = $this->session->userdata('id_pengguna');
          if( $a == ''){ ?>
            <li><a href="<?=base_url('publik/login')?>">Login</a></li> 
          <!-- <li><a href="https://blksmg1.disnakertrans.jatengprov.go.id/korea/">Login</a></li>   -->
          <li><a href="<?=base_url('publik/register')?>">Register</a></li>
         <?php  }else{ ?>
          <li><a href="<?=base_url('publik/dashboard')?>">Daftar Pelatihan</a></li>
          <li><a href="<?=base_url('publik/riwayat')?>">Riwayat Pelatihan</a></li> 
          <li><a target="BLANK_" href="https://blksmg1.disnakertrans.jatengprov.go.id/korea/">Korea</a></li>
          <li><a href="<?=base_url('publik/profil')?>"><?=data_user($a)->username?></a> </li> 
          <li ><a href="<?=base_url('publik/open_notif/')?>"><i style="color:red" class="icofont-alarm"></i><?=count_notif($a)?></a></li> 
          <li><a href="<?=base_url('portal/logout')?>">Logout</a></li> 

          <?php  } ?>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->