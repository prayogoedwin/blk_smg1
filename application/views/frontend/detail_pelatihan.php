  <!-- Select2 -->
  <!-- <link rel="stylesheet" href="<?php echo base_url() ?>assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/adminlte/bower_components/select2/dist/css/select2.min.css"> -->
 <!-- ======= Contact Section ======= -->
 <section id="kontak" class="contact">
      <div class="container" data-aos="fade-up">
          <br/>
          <br/>
          <br/>

        <div class="section-title">
          <h2>Detail Pelatihan <?=$pelatihan->nama_pelatihan?></h2>
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>

       
        <div class="row">


          <div class="col-lg-12 mt-5 mt-lg-0">

            <!-- <form action="forms/contact.php" method="post" role="form" class="php-email-form"> -->
            <?php 
             $info = $this->session->flashdata('info');
             $pesan = $this->session->flashdata('message');
            echo form_open_multipart('publik/aksi_register_pelatihan', 'class=""');   
                                  
            if( $info == 'danger'){ ?>
                
                <span style="color:red"><?=$pesan?> </span>
                
            <?php } ?>

            <?php if( $info == 'success'){ ?>
                                      
              <span style="color:green"> <?=$pesan?> </span>
              
          <?php } ?>

              <div class="form-row">
                <div class="col-md-12 form-group">
                Deskripsi
                <textarea disabled class="form-control" rows="5"><?=$pelatihan->deskripsi?></textarea>
                  <!-- <input type="text" name="kuota" class="form-control" id="name" placeholder="Kuota Kelas" data-rule="required:true"  value="<?=$pelatihan->kuota_kelas?>"/> -->
                  <div class="validate"></div>
                </div>
                
                
              </div>

              <input type="hidden" class="form-control" name="id_pelatihan"  value="<?=$pelatihan->id?>">

              <div class="form-row">
              <div class="col-md-6 form-group">
                    Tanggal Pembukaan Pendaftaran
                  <input type="text" disabled class="form-control" name="t_p_p"  value="<?=$this->formatter->getDateMonthFormatUser($pelatihan->tgl_pembukaan_pendaftaran)?>"/>
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                    Tanggal Penutupan Pendaftaran
                  <input type="text" disabled class="form-control" name="t_p_p"  value="<?=$this->formatter->getDateMonthFormatUser($pelatihan->tgl_penutupan_pendaftaran)?>"/>
                  <div class="validate"></div>
                </div>
              </div>

              <div class="form-row">
              <div class="col-md-6 form-group">
                    Tanggal Kelas Mulai
                  <input type="text" disabled class="form-control" name="t_p_p"  value="<?=$this->formatter->getDateMonthFormatUser($pelatihan->tgl_kelas_mulai)?>"/>
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                    Tanggal Kelas Berakhir
                  <input type="text" disabled class="form-control" name="t_p_p"  value="<?=$this->formatter->getDateMonthFormatUser($pelatihan->tgl_kelas_selesai)?>"/>
                  <div class="validate"></div>
                </div>
              </div>

            
              
              <!--<?php echo count($quesioner_umum_s) > 0 ? '<br/><br/><hr/><h2><u>Pertanyaan Umum</u></h2>' : ''; ?>-->
              <!--<?php echo count($quesioner_umum_s) > 0 ? '<hr style="border: 1px solid black;">' : ''; ?>-->
              
              <?php foreach($quesioner_umum_s as $qumum): 
              $hide = NULL;
              if($qumum->quesioner == 1){
                $hide = '';
                $requred = 'required';
              }else{
                $hide = 'hidden';
                $requred = '';
              }
              ?>

              
              <?php if($qumum->tipe_jawaban == ''){ ?>

                <div class="form-row" <?=$hide?>>
                <div class="col-md-12 form-group">
                      <?=$qumum->pertanyaan?>
                    <input <?=$requred?> type="text" class="form-control" name="jawaban<?=$qumum->id?>"  value=""/>
                    <div class="validate"></div>
                  </div>
                  
                </div>

              <?php }elseif($qumum->tipe_jawaban == '1'){ ?>

                <div class="form-row" <?=$hide?>>
                <div class="col-md-12 form-group">
                <?=$qumum->pertanyaan?>
                  <select <?=$requred?> class="form-control" name="jawaban<?=$qumum->id?>"   >
                  <?php foreach(data_pilihan($qumum->id) as $umum): ?>
                  <option value="<?=$umum->id?>"><?=$umum->pilihan?></option>
                  <?php endforeach;?>
                  </select>
                  <div class="validate"></div>
                </div>
                
              </div>


              <?php }elseif($qumum->tipe_jawaban == '2'){ ?>

                <div class="form-row" <?=$hide?>>
                <div class="col-md-12 form-group">
                  <?=$qumum->pertanyaan?>
                  <select <?=$requred?> class="form-control" name="jawaban<?=$qumum->id?>"   > 
                  <option   value="Tidak">Tidak</option>
                  <option   value="Ya">Ya</option>
                  </select>
                  <div class="validate"></div>
                </div>
                
              </div>


              


              <?php }elseif($qumum->tipe_jawaban == '3'){ ?>

                <div class="form-row" <?=$hide?> >
                <div class="col-md-12 form-group" style="border-style: ridge;">
                <?=$qumum->pertanyaan?><br/>
                  <?php foreach(data_pilihan($qumum->id) as $umum): ?>

                    <!-- <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                    <label class="form-check-label" for="flexCheckIndeterminate">
                      Indeterminate checkbox
                    </label>
                  </div> -->

                  
                  <input type="checkbox" name="jawaban<?=$umum->id?>[]" value="<?=$umum->pilihan?>">&nbsp;<?=$umum->pilihan?><br/>
                  <?php endforeach;?>
                 

                  <div class="validate"></div>
                </div>

                </div>


              <?php }?>



              

              

              <?php endforeach; ?>
              

              
              
              <!--<?php echo count($quesioner_demografi_s) > 0 ? '<br/><br/><hr/><h2><u>Pertanyaan Kondisi Demografi Sosial Ekonomi</u></h2>' : ''; ?>-->
              <!--<?php echo count($quesioner_demografi_s) > 0 ? '<hr style="border: 1px solid black;">' : ''; ?>-->
              <?php foreach($quesioner_demografi as $qdemo): 
              $hide = NULL;
              if($qdemo->quesioner == 1){
                $hide = '';
                $requred = 'required';
              }else{
                $hide = 'hidden';
                $requred = '';
              }
              ?>

              <?php if($qdemo->tipe_jawaban == ''){ ?>

              <div class="form-row" <?=$hide?>>
              <div class="col-md-12 form-group">
                    <?=$qdemo->pertanyaan?>
                  <input <?=$requred?> type="text" class="form-control" name="jawaban<?=$qdemo->id?>"  value=""/>
                  <div class="validate"></div>
                </div>
                
              </div>

              <?php }elseif($qdemo->tipe_jawaban == '1'){ ?>

              <div class="form-row" <?=$hide?>>
              <div class="col-md-12 form-group">
                <?=$qdemo->pertanyaan?>
                <select <?=$requred?> class="form-control" name="jawaban<?=$qdemo->id?>"   >
                <?php foreach(data_pilihan($qdemo->id) as $demo): ?>
                <option value="<?=$demo->id?>"><?=$demo->pilihan?></option>
                <?php endforeach;?>
                </select>
                <div class="validate"></div>
              </div>

              </div>


              <?php }elseif($qdemo->tipe_jawaban == '2'){ ?>

              <div class="form-row" <?=$hide?>>
              <div class="col-md-12 form-group">
                <?=$qdemo->pertanyaan?>
                <select <?=$requred?> class="form-control" name="jawaban<?=$qdemo->id?>"   >
                <option   value="Ya">Ya</option>
                <option   value="Tidak">Tidak</option>
                </select>
                <div class="validate"></div>
              </div>

              </div>


              <?php }?>

              <?php endforeach; ?>

              
              
             <!--<?php echo count($quesioner_minat_s) > 0 ? '<br/><br/><hr/><h2><u>Pertanyaan Minat & Bakat</u></h2>' : ''; ?> -->
             <!-- <?php echo count($quesioner_minat_s) > 0 ? '<hr style="border: 1px solid black;">' : ''; ?>-->
              <?php foreach($quesioner_minat as $qminat): 
              $hide = NULL;
              if($qminat->quesioner == 1){
                $hide = '';
                $requred = 'required';
              }else{
                $hide = 'hidden';
                $requred = '';
              }
              ?>

              <?php if($qminat->tipe_jawaban == ''){ ?>

              <div class="form-row" <?=$hide?>>
              <div class="col-md-12 form-group">
                    <?=$qminat->pertanyaan?>
                  <input <?=$requred?> type="text" class="form-control" name="jawaban<?=$qminat->id?>"  value=""/>
                  <div class="validate"></div>
                </div>
                
              </div>

              <?php }elseif($qminat->tipe_jawaban == '1'){ ?>

              <div class="form-row" <?=$hide?>>
              <div class="col-md-12 form-group">
                <?=$qminat->pertanyaan?>
                <select <?=$requred?> class="form-control" name="jawaban<?=$qminat->id?>"   >
                <?php foreach(data_pilihan($qminat->id) as $minat): ?>
                <option value="<?=$minat->id?>"><?=$minat->pilihan?></option>
                <?php endforeach;?>
                </select>
                <div class="validate"></div>
              </div>

              </div>


              <?php }elseif($qminat->tipe_jawaban == '2'){ ?>

              <div class="form-row" <?=$hide?>>
              <div class="col-md-12 form-group">
                <?=$qminat->pertanyaan?>
                <select <?=$requred?> class="form-control" name="jawaban<?=$qminat->id?>"   >
                <option   value="Ya">Ya</option>
                <option   value="Tidak">Tidak</option>
                </select>
                <div class="validate"></div>
              </div>

              </div>


              <?php }?>

              <?php endforeach; ?>



              <!--<?php echo count($quesioner_khusus_s) > 0 ? '<br/><br/><hr/><h2><u>Pertanyaan Khusus</u></h2>' : ''; ?>-->
              <!--<?php echo count($quesioner_khusus_s) > 0 ? '<hr style="border: 1px solid black;">' : ''; ?>-->
              <?php foreach($quesioner_khusus as $qkhusus): 
              $hide = NULL;
              if($qkhusus->quesioner == 1){
                $hide = '';
                $requred = 'required';
              }else{
                $hide = 'hidden';
                $requred = '';
              }
              ?>

              <?php if($qkhusus->tipe_jawaban == ''){ ?>

              <div class="form-row" <?=$hide?>>
              <div class="col-md-12 form-group">
                    <?=$qkhusus->pertanyaan?>
                  <input <?=$requred?> type="text" class="form-control" name="jawaban<?=$qkhusus->id?>"  value=""/>
                  <div class="validate"></div>
                </div>
                
              </div>

              <?php }elseif($qkhusus->tipe_jawaban == '1'){ ?>

              <div class="form-row" <?=$hide?>>
              <div class="col-md-12 form-group">
                <?=$qkhusus->pertanyaan?>
                <select <?=$requred?> class="form-control" name="jawaban<?=$qkhusus->id?>"   >
                <?php foreach(data_pilihan($qkhusus->id) as $khusus): ?>
                <option value="<?=$khusus->id?>"><?=$khusus->pilihan?></option>
                <?php endforeach;?>
                </select>
                <div class="validate"></div>
              </div>

              </div>


              <?php }elseif($qkhusus->tipe_jawaban == '2'){ ?>

              <div class="form-row" <?=$hide?>>
              <div class="col-md-12 form-group">
                <?=$qkhusus->pertanyaan?>
                <select <?=$requred?> class="form-control" name="jawaban<?=$qkhusus->id?>"   >
                <option   value="Ya">Ya</option>
                <option   value="Tidak">Tidak</option>
                </select>
                <div class="validate"></div>
              </div>

              </div>


              <?php }elseif($qkhusus->tipe_jawaban == '3'){ ?>

              <div class="form-row" <?=$hide?> >
              <div class="col-md-12 form-group" style="border-style: ridge;">
              <?=$qkhusus->pertanyaan?><br/>
                <?php foreach(data_pilihan($qkhusus->id) as $khusus): ?>

                  <!-- <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                  <label class="form-check-label" for="flexCheckIndeterminate">
                    Indeterminate checkbox
                  </label>
                </div> -->

                <input type="checkbox" name="jawaban<?=$khusus->id?>[]" value="<?=$khusus->pilihan?>">&nbsp;<?=$khusus->pilihan?><br/>

                
                
                <?php endforeach;?>
               
                <div class="validate"></div>
              </div>

              </div>


            <?php }?>

              <?php endforeach; ?>


             
              
              <?php echo count($quesioner_upload_s) > 0 ? '<br/><br/><hr/><h2><u>Upload Dokumen</u></h2>' : ''; ?>
              <?php foreach($quesioner_upload as $qupload): 
                
                $hide = NULL;
              if($qupload->quesioner == 1){
                $hide = '';
                $requred = 'required';
              }else{
                $hide = 'hidden';
                $requred = '';
              }
              ?>

              <div class="form-row" <?=$hide?>>
              <div class="col-md-12 form-group">
                    <?=$qupload->pertanyaan?>
                  <input <?=$requred?> type="file" class="form-control h-auto" name="file<?=$qupload->id?>"  value=""/>
                  <div class="validate"></div>
                </div>
                
              </div>

              <?php endforeach; ?>

              
              
              <div class="text-center"><button class="btn btn-success" type="submit">Daftar Pelatihan Sekarang</button></div>
              <?php echo form_close(); ?>

          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

    <?php
  $this->load->view('frontend/a_footer_acc');
  ?>

  <!-- Select2 -->
<!-- <script src="<?php echo base_url() ?>assets/adminlte/bower_components/select2/dist/js/select2.full.min.js"></script> -->

<!-- <script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  })
</script> -->