   <!-- ======= Footer ======= -->
   <footer id="footer" style="background-color:#D00000">

<!-- <div class="footer-newsletter">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <h4>Our Newsletter</h4>
        <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
      </div>
      <div class="col-lg-6">
        <form action="" method="post">
          <input type="email" name="email"><input type="submit" value="Subscribe">
        </form>
      </div>
    </div>
  </div>
</div> -->

<!-- <div class="footer-top">
  <div class="container">
    <div class="row">

      <div class="col-lg-3 col-md-6 footer-links">
        <h4>Useful Links</h4>
        <ul>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
        </ul>
      </div>

      <div class="col-lg-3 col-md-6 footer-links">
        <h4>Our Services</h4>
        <ul>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
        </ul>
      </div>

      <div class="col-lg-3 col-md-6 footer-contact">
        <h4>Contact Us</h4>
        <p>
          A108 Adam Street <br>
          New York, NY 535022<br>
          United States <br><br>
          <strong>Phone:</strong> +1 5589 55488 55<br>
          <strong>Email:</strong> info@example.com<br>
        </p>

      </div>

      <div class="col-lg-3 col-md-6 footer-info">
        <h3>About Eterna</h3>
        <p>Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna derita valies darta donna mare fermentum iaculis eu non diam phasellus.</p>
        <div class="social-links mt-3">
          <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
          <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
          <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
          <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
          <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
        </div>
      </div>

    </div>
  </div>
</div> -->

<div class="container" style="text-align:center">
  <div class="copyright">
    <!-- &copy; Copyright <strong> 2021 <span>BLK Semarang 1</span></strong>. All Rights Reserved -->
    <a href="<?=base_url()?>#faq" style="color:white">Bantuan</a> dan <a href="<?=base_url()?>#kontak" style="color:white">Kontak</a>
    <br/>
    <br/>

    <div style="letter-spacing:15px">
    <a style="color:white" href="https://www.facebook.com/balailatihankerjasemarang1/"><i class="fa fa-facebook fa-2x"></i></a>  <a style="color:white" href="https://www.instagram.com/blksemarang_1/"><i class="fa fa-instagram fa-2x"></i></a>  
    <!-- <i class="fa fa-youtube fa-2x"></i>  <i class="fa fa-twitter fa-2x"></i> -->
    </div>
  </div>
  <div class="credits">
    <!-- All the links in the footer should remain intact. -->
    <!-- You can delete the links only if you purchased the pro version. -->
    <!-- Licensing information: https://bootstrapmade.com/license/ -->
    <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/eterna-free-multipurpose-bootstrap-template/ -->
    <!-- Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
  </div>
</div>
</footer><!-- End Footer -->

  
  <!-- Vendor JS Files -->
  <script src="<?=base_url()?>blk_smg/satu/assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?=base_url()?>blk_smg/satu/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url()?>blk_smg/satu/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?=base_url()?>blk_smg/satu/assets/vendor/php-email-form/validate.js"></script>
  <script src="<?=base_url()?>blk_smg/satu/assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="<?=base_url()?>blk_smg/satu/assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="<?=base_url()?>blk_smg/satu/assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="<?=base_url()?>blk_smg/satu/assets/vendor/counterup/counterup.min.js"></script>
  <script src="<?=base_url()?>blk_smg/satu/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?=base_url()?>blk_smg/satu/assets/vendor/venobox/venobox.min.js"></script>

  <!-- Template Main JS File -->
  <!--<script src="<?=base_url()?>blk_smg/satu/assets/js/main.js"></script>-->