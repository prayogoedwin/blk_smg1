  <!-- ======= Hero Section ======= -->
  <section id="hero" style="height:400px;">
    <div class="hero-container">
      <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

        <!-- <ol class="carousel-indicators" id="hero-carousel-indicators"></ol> -->

        <div class="carousel-inner" >

          <!-- Slide 1 -->
          <div class="carousel-item active" >
          
             
              <img src="<?=base_url()?>upload/skill.png" alt="Los Angeles" class="img-responsive" style="width:100%">
              
              <a href="" class="btn btn-get-started">Read More</a>
             
           
          </div>

            <!-- Slide 1 -->
           <div class="carousel-item " >
           
              
                <img src="<?=base_url()?>upload/skill3.png" alt="Los Angeles" class="img-responsive" style="width:100%">
                <a href="" class="btn btn-get-started">Read More</a>
                
           
          </div>

          <!-- Slide 2 -->
          <!-- <div class="carousel-item" style="background: url(assets/img/slide/slide-2.jpg)">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 class="animate__animated fanimate__adeInDown">Lorem <span>Ipsum Dolor</span></h2>
                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                <a href="" class="btn-get-started animate__animated animate__fadeInUp">Read More</a>
              </div>
            </div>
          </div> -->

          <!-- Slide 3 -->
          <!-- <div class="carousel-item" style="background: url(assets/img/slide/slide-3.jpg)">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 class="animate__animated animate__fadeInDown">Sequi ea <span>Dime Lara</span></h2>
                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                <a href="" class="btn-get-started animate__animated animate__fadeInUp">Read More</a>
              </div>
            </div>
          </div> -->

        </div>

        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon icofont-rounded-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon icofont-rounded-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
  </section><!-- End Hero -->


    <main id="main">

    <!-- ======= Featured Section ======= -->
    <section id="pelatihan" class="featured">
      <div class="container">
   

        <div class="section-title" data-aos="fade-up">
          <h2>Daftar Pelatihan 2021</h2>
        </div>

        <div class="row">
          <div class="col-lg-4">
            <div class="icon-box">
              <!-- <i class="icofont-learn"></i> -->
              <h3><a href="">Tour Guide</a></h3>
              <p>5 Februari 2021 - 10 Februari 2021</p>
              <div class="expanded-text">
                <span class="short-name">Lihat Detail</span><br/>
                <span class="longer-name">On The Origin Of Species By Means Of 
                  Natural Selection, Or The Preservation Of Favoured Races In 
                  The Struggle For Life</span>
                  <a href="" type="button" class="btn" style="background-color:#fff; float: right; border-color:#D00000;">Daftar Sekarang</a>
                 
                </div>
                

                <br/>
            </div>
          </div>
          <div class="col-lg-4 mt-4 mt-lg-0">
            <div class="icon-box">
              <!-- <i class="icofont-coffee-pot"></i> -->
              <h3><a href="">Barista</a></h3>
              <p>21 Agustus 2021 - 25 Agustus 2021</p>
              <div class="expanded-text">
              <span class="short-name">Lihat Detail</span><br/>
              <span class="longer-name">On The Origin Of Species By Means Of 
                Natural Selection, Or The Preservation Of Favoured Races In 
                The Struggle For Life</span>
                <a href="" type="button" class="btn" style="background-color:#fff; float: right; border-color:#D00000;">Daftar Sekarang</a>
                 
              </div>
              
              <br/>
            </div>
          </div>
          <div class="col-lg-4 mt-4 mt-lg-0">
            <div class="icon-box">
              <!-- <i class="icofont-tasks-alt"></i> -->
              <h3><a href="">Bahasa Inggris</a></h3>
              <p>8 Oktober 2021 - 11 Oktober 2021</p>
              <div class="expanded-text">
                <span class="short-name">Lihat Detail</span><br/>
                <span class="longer-name">On The Origin Of Species By Means Of 
                  Natural Selection, Or The Preservation Of Favoured Races In 
                  The Struggle For Life</span>
                  <a href="" type="button" class="btn" style="background-color:#fff; float: right; border-color:#D00000;">Daftar Sekarang</a>
                 
                </div>

                <br/>
            </div>
          </div>
        </div>
        <br/>
        <div class="row">
          <div class="col-lg-4">
            <div class="icon-box">
              <!-- <i class="icofont-learn"></i> -->
              <h3><a href="">Tour Guide</a></h3>
              <p>5 Februari 2021 - 10 Februari 2021</p>
              <div class="expanded-text">
                <span class="short-name">Lihat Detail</span><br/>
                <span class="longer-name">On The Origin Of Species By Means Of 
                  Natural Selection, Or The Preservation Of Favoured Races In 
                  The Struggle For Life</span>
                  <a href="" type="button" class="btn" style="background-color:#fff; float: right; border-color:#D00000;">Daftar Sekarang</a>
                 
                </div>

                <br/>
            </div>
          </div>
          <div class="col-lg-4 mt-4 mt-lg-0">
            <div class="icon-box">
              <!-- <i class="icofont-coffee-pot"></i> -->
              <h3><a href="">Barista</a></h3>
              <p>21 Agustus 2021 - 25 Agustus 2021</p>
              <div class="expanded-text">
              <span class="short-name">Lihat Detail</span><br/>
              <span class="longer-name">On The Origin Of Species By Means Of 
                Natural Selection, Or The Preservation Of Favoured Races In 
                The Struggle For Life</span>
                <a href="" type="button" class="btn" style="background-color:#fff; float: right; border-color:#D00000;">Daftar Sekarang</a>
                 
              </div>
              
              <br/>
            </div>
          </div>
          <div class="col-lg-4 mt-4 mt-lg-0">
            <div class="icon-box">
              <!-- <i class="icofont-tasks-alt"></i> -->
              <h3><a href="">Bahasa Inggris</a></h3>
              <p>8 Oktober 2021 - 11 Oktober 2021</p>
              <div class="expanded-text">
                <span class="short-name">Lihat Detail</span><br/>
                <span class="longer-name">On The Origin Of Species By Means Of 
                  Natural Selection, Or The Preservation Of Favoured Races In 
                  The Struggle For Life</span>
                  <a href="" type="button" class="btn" style="background-color:#fff; float: right; border-color:#D00000;">Daftar Sekarang</a>
                 
                </div>

                <br/>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Featured Section -->


    <section id="karir" class="team" style="background-color: #D00000; color:#fff">
      <div class="container">

        <br/>
        <br/>
        <br/>

        <div class="section-title" data-aos="fade-up">
          <h2>Karir</h2>
        </div>

        <div class="row">
         
          <table id="example2" class="table table-bordered table-striped" style="color:#fff">
            <thead>
            <tr style="text-align: center;">
              <th>No</th>
              <th>Lowongan</th>
              <th>Kab/Kota</th>
              <th>Tanggal Tutup Lowongan</th>
            </tr>
            </thead>
            <tbody>
           
              <tr>
              <td>1</td>
               <td>Barista - PT AMPKRT</td>
               <td>Semarang</td>
               <td>22 Maret 2021</td>
              </tr>


              <tr>
                 <td>2</td>
                 <td>Tour Guide - PT AUAD</td>
                 <td>Kudus</td>
                 <td>21 Februari 2021</td>
              </tr>


              <tr>
                   <td>3</td>
                   <td>Guru Bahasa Inggris - PRIMAGAMI</td>
                   <td>Demak</td>
                   <td>14 Mei 2021</td>
              </tr>

              <tr>
                  <td>4</td>
                  <td>Barista - PT AMPKRT</td>
                  <td>Semarang</td>
                  <td>22 Maret 2021</td>
              </tr>

              <tr>
                  <td>5</td>
                  <td>Tour Guide - PT AUAD</td>
                 <td>Kudus</td>
                 <td>21 Februari 2021</td>
              </tr>


              <tr>
                <td>6</td>
                 <td>Barista - PT AMPKRT</td>
                 <td>Semarang</td>
                 <td>22 Maret 2021</td>
                </tr>
  
  
                <tr>
                   <td>7</td>
                   <td>Tour Guide - PT AUAD</td>
                   <td>Kudus</td>
                   <td>21 Februari 2021</td>
                </tr>
  
  
                <tr>
                     <td>8</td>
                     <td>Guru Bahasa Inggris - PRIMAGAMI</td>
                     <td>Demak</td>
                     <td>14 Mei 2021</td>
                </tr>
  
                <tr>
                    <td>9</td>
                    <td>Barista - PT AMPKRT</td>
                    <td>Semarang</td>
                    <td>22 Maret 2021</td>
                </tr>
  
                <tr>
                    <td>10</td>
                    <td>Tour Guide - PT AUAD</td>
                   <td>Kudus</td>
                   <td>21 Februari 2021</td>
                </tr>
               
               
            
     

          </tbody>
          </table>

        </div>

      </div>
    </section><!-- End Team Section -->


    <section id="alumni" class="team">
      <div class="container">

        <br/>
        <br/>
        <br/>

        <div class="section-title" data-aos="fade-up">
          <h2>Alumni</h2>
        </div>

        <div class="row">
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch" >
            <div class="member">
              <img src="<?=base_url()?>blk_smg/satu/assets/img/team/team-1.jpg" alt="">
              <h4 style="color:#D00000">Walter White</h4>
              <!-- <span>CTO</span> -->
              <p>
                Menjadi Barista di PT XYZ 10 Oktober 2019
              </p>
              <!-- <div class="social">
                <a href=""><i class="icofont-twitter"></i></a>
                <a href=""><i class="icofont-facebook"></i></a>
                <a href=""><i class="icofont-instagram"></i></a>
                <a href=""><i class="icofont-linkedin"></i></a>
              </div> -->
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
              <img src="<?=base_url()?>blk_smg/satu/assets/img/team/team-2.jpg" alt="">
              <h4 style="color:#D00000">Sarah Jhinson</h4>
              <!-- <span>CTO</span> -->
              <p>
                Menjadi Guru Private Bahasa 10 Oktober 2019
              </p>
              <!-- <div class="social">
                <a href=""><i class="icofont-twitter"></i></a>
                <a href=""><i class="icofont-facebook"></i></a>
                <a href=""><i class="icofont-instagram"></i></a>
                <a href=""><i class="icofont-linkedin"></i></a>
              </div> -->
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
              <img src="<?=base_url()?>blk_smg/satu/assets/img/team/team-3.jpg" alt="">
              <h4 style="color:#D00000">William Anderson</h4>
              <!-- <span>CTO</span> -->
              <p>
                Menjadi Tour Guide di PT XYZ 10 Oktober 2019
              </p>
              <!-- <div class="social">
                <a href=""><i class="icofont-twitter"></i></a>
                <a href=""><i class="icofont-facebook"></i></a>
                <a href=""><i class="icofont-instagram"></i></a>
                <a href=""><i class="icofont-linkedin"></i></a>
              </div> -->
            </div>
          </div>

        </div>
        <br/>
        <div class="row">
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
              <img src="<?=base_url()?>blk_smg/satu/assets/img/team/team-1.jpg" alt="">
              <h4 style="color:#D00000">Walter White</h4>
              <!-- <span>CTO</span> -->
              <p>
                Menjadi Barista di PT XYZ 10 Oktober 2019
              </p>
              <!-- <div class="social">
                <a href=""><i class="icofont-twitter"></i></a>
                <a href=""><i class="icofont-facebook"></i></a>
                <a href=""><i class="icofont-instagram"></i></a>
                <a href=""><i class="icofont-linkedin"></i></a>
              </div> -->
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
              <img src="<?=base_url()?>blk_smg/satu/assets/img/team/team-2.jpg" alt="">
              <h4 style="color:#D00000">Sarah Jhinson</h4>
              <!-- <span>CTO</span> -->
              <p>
                Menjadi Guru Private Bahasa 10 Oktober 2019
              </p>
              <!-- <div class="social">
                <a href=""><i class="icofont-twitter"></i></a>
                <a href=""><i class="icofont-facebook"></i></a>
                <a href=""><i class="icofont-instagram"></i></a>
                <a href=""><i class="icofont-linkedin"></i></a>
              </div> -->
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
              <img src="<?=base_url()?>blk_smg/satu/assets/img/team/team-3.jpg" alt="">
              <h4 style="color:#D00000">William Anderson</h4>
              <!-- <span>CTO</span> -->
              <p>
                Menjadi Tour Guide di PT XYZ 10 Oktober 2019
              </p>
              <!-- <div class="social">
                <a href=""><i class="icofont-twitter"></i></a>
                <a href=""><i class="icofont-facebook"></i></a>
                <a href=""><i class="icofont-instagram"></i></a>
                <a href=""><i class="icofont-linkedin"></i></a>
              </div> -->
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Team Section -->


    <section id="faq" class="team" style="background-color: #D00000;">
      <div class="container">

        <br/>
        <br/>
        <br/>

        <div class="section-title" style="color:#FFF" data-aos="fade-up">
          <h2>Tanya Jawab Umum</h2>
        </div>

        <div class="row">

          <div class="card card-lg" style="width: 100%;">
            
            <div class="accordion accordion-type-2 accordion-flush" id="accordion_2">
                <div class="card">
                    <div class="card-header d-flex justify-content-between activestate">
                        <a role="button" data-toggle="collapse" href="#collapse_1i" aria-expanded="true">The Intellectual Property</a>
                    </div>
                    <div id="collapse_1i" class="collapse show" data-parent="#accordion_2" role="tabpanel">
                        <div class="card-body pa-15">The Intellectual Property disclosure will inform users that the contents, logo and other visual media you created is your property and is protected by copyright laws.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <a class="collapsed" role="button" data-toggle="collapse" href="#collapse_2i" aria-expanded="false">Termination clause</a>
                    </div>
                    <div id="collapse_2i" class="collapse" data-parent="#accordion_2">
                        <div class="card-body pa-15">A Termination clause will inform that users’ accounts on your website and mobile app or users’ access to your website and mobile (if users can’t have an account with you) can be terminated in case of abuses or at your sole discretion.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <a class="collapsed" role="button" data-toggle="collapse" href="#collapse_3i" aria-expanded="false">Governing Law</a>
                    </div>
                    <div id="collapse_3i" class="collapse" data-parent="#accordion_2">
                        <div class="card-body pa-15">A Governing Law will inform users which laws govern the agreement. This should the country in which your company is headquartered or the country from which you operate your website and mobile app.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <a class="collapsed" role="button" data-toggle="collapse" href="#collapse_4i" aria-expanded="false">Limit what users can do</a>
                    </div>
                    <div id="collapse_4i" class="collapse" data-parent="#accordion_2">
                        <div class="card-body pa-15">A Limit What Users Can Do clause can inform users that by agreeing to use your service, they’re also agreeing to not do certain things. This can be part of a very long and thorough list in your Terms and Conditions agreements so as to encompass the most amount of negative uses.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <a class="collapsed" role="button" data-toggle="collapse" href="#collapse_5i" aria-expanded="false">Limitation of liability of your products</a>
                    </div>
                    <div id="collapse_5i" class="collapse" data-parent="#accordion_2">
                        <div class="card-body pa-15">No matter what kind of goods you sell, best practices direct you to present any warranties you are disclaiming and liabilities you are limiting in a way that your customers will notice.</div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <a class="collapsed" role="button" data-toggle="collapse" href="#collapse_6i" aria-expanded="false">How to enforce Terms and Conditions</a>
                    </div>
                    <div id="collapse_6i" class="collapse" data-parent="#accordion_2">
                        <div class="card-body pa-15">While creating and having a Terms and Conditions is important, it’s far more important to understand how you can make the Terms and Conditions enforceable. You should always use clickwrap to get users to agree to your Terms and Conditions. Clickwrap is when you make your users take some action – typically clicking something – to show they’re agreeing. Here’s how Engine Yard uses the clickwrap agreement with the I agree check box:</div>
                    </div>
                </div>
            </div>

        </div>
         

        </div>

      </div>
    </section><!-- End Team Section -->

     <!-- ======= Contact Section ======= -->
     <section id="kontak" class="contact">
      <div class="container" data-aos="fade-up">
          <br/>
          <br/>
          <br/>

        <div class="section-title">
          <h2>Kontak</h2>
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>

        <div>
          <iframe style="border:0; width: 100%; height: 270px;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" allowfullscreen></iframe>
        </div>

        <div class="row mt-5">

          <div class="col-lg-4">
            <div class="info">
              <div class="address">
                <i class="icofont-google-map"></i>
                <h4>Location:</h4>
                <p>A108 Adam Street, New York, NY 535022</p>
              </div>

              <div class="email">
                <i class="icofont-phone"></i>
                <h4>Telepon:</h4>
                <p>024 00000000</p>
              </div>

              <div class="phone">
                <i class="icofont-whatsapp"></i>
                <h4>Whatsapp:</h4>
                <p>08112312312</p>
              </div>

            </div>

          </div>

          <div class="col-lg-8 mt-5 mt-lg-0">

            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="form-row">
                <div class="col-md-6 form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                <div class="validate"></div>
              </div>
              <div class="mb-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>

          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  


  <?php
  $this->load->view('frontend/a_footer');
  ?>