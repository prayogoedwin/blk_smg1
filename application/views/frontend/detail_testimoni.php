 <!-- ======= Contact Section ======= -->
 <section id="kontak" class="contact">
      <div class="container" data-aos="fade-up">
          <br/>
          <br/>
          <br/>

        <div class="section-title">
          <h2>Testimoni <?=$pendaftaran->nama?> Untuk Pelatihan <?=$pelatihan->nama_pelatihan?></h2>
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>

       
        <div class="row">


          <div class="col-lg-12 mt-5 mt-lg-0">

            <!-- <form action="forms/contact.php" method="post" role="form" class="php-email-form"> -->
            <?php 
             $info = $this->session->flashdata('info');
             $pesan = $this->session->flashdata('message');
              
                                  
            if( $info == 'danger'){ ?>
                
                <span style="color:red"><?=$pesan?> </span>
                
            <?php } ?>

            <?php if( $info == 'success'){ ?>
                                      
              <span style="color:green"> <?=$pesan?> </span>
              
          <?php } ?>

          <div class="row text-justify" >

         
          <p><?=$pendaftaran->testimoni?></p>
          
          <?php if($pendaftaran->video != ''){ ?>
            <iframe width="100%" height="500" src="https://www.youtube.com/embed/<?=$this->uri->segment(5)?>?controls=0"  frameborder="0" allowfullscreen></iframe>

          <?php } ?>
          

          </div>


            

            
              
              

              
              
              <!-- <div class="text-center"><button class="btn btn-success" type="submit">Kirim Testimoni</button></div> -->
             

          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

    <?php
  $this->load->view('frontend/a_footer_acc');
  ?>