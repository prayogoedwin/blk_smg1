 <!-- ======= Contact Section ======= -->
 <section id="kontak" class="contact">
      <div class="container" data-aos="fade-up">
          <br/>
          <br/>
          <br/>

        <div class="section-title">
          <h2>Profil Pengguna</h2>
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>

       
        <div class="row">


          <div class="col-lg-12 mt-5 mt-lg-0">

            <!-- <form action="forms/contact.php" method="post" role="form" class="php-email-form"> -->
            <?php 
             $info = $this->session->flashdata('info');
             $pesan = $this->session->flashdata('message');
            echo form_open('publik/aksi_register', 'class=""');   
                                  
            if( $info == 'danger'){ ?>
                
                <span style="color:red"><?=$pesan?> </span>
                
            <?php } ?>

            <?php if( $info == 'success'){ ?>
                                      
              <span style="color:green"><?=$pesan?> </span>
              
          <?php } ?>

              <div class="form-row">
                <div class="col-md-6 form-group">
                Nama Lengkap
                  <input type="text" name="name" class="form-control" id="name" placeholder="Nama Lengkap" data-rule="required:true" data-msg="Nama Tidak boleh kosong"  value="<?=$profil->nama?>"/>
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                NIK
                  <input type="text" name="nik" class="form-control" id="nik" placeholder="NIK" data-rule="minlen:16" data-msg="Minimal 16 Digit" value="<?=$profil->nik?>" />
                  <div class="validate"></div>
                </div>
                
              </div>

              <div class="form-row">
              <div class="col-md-6 form-group">
                    Email
                  <input type="email" class="form-control" name="email" id="email" placeholder="Email Aktif" data-rule="email" data-msg="Mohon input email yang valid" value="<?=$profil->username?>" />
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                    Password
                <input type="password" name="password" class="form-control" id="password" placeholder="Password" data-rule="minlen:6" data-msg="Minimal 6 Karakter" value="<?=$profil->password?>" />
                  <div class="validate"></div>
                </div>
              </div>

              <div class="form-row">
                <div class="col-md-6 form-group">
                    No Whatasapp
                  <input type="text" name="wa" class="form-control" id="wa" placeholder="No Whatsapp Aktif" data-rule="minlen:10" data-msg="Minimal nomor WA 10 Digit"  value="<?=$profil->wa?>" />
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                    No HP
                  <input type="text" class="form-control" name="hp" id="hp" placeholder="No HP Aktif" data-rule="minlen:10" data-msg="Minimal nomor HP 10 Digit" value="<?=$profil->hp?>" />
                  <div class="validate"></div>
                </div>
              </div>

              <div class="form-row">
                <div class="col-md-6 form-group">
                    Tempat Lahir
                  <input type="text" name="tempat" class="form-control" id="tempat" placeholder="Tempat Lahir"  data-rule="required" data-msg="Tidak Boleh Kosong"  value="<?=$profil->tempat_lahir?>" />
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                    Tanggal Lahir
                  <input type="date" class="form-control" name="tanggal" id="tanggal" placeholder=""  data-rule="required" data-msg="Tidak Boleh Kosong" value="<?=$profil->tgl_lahir?>" />
                  <div class="validate"></div>
                </div>
              </div>

              <div class="form-row">
                <div class="col-md-6 form-group">
                     Kota Domisili
                  
                  <select class="form-control" name="domisili" id="domisili" required >
                  
                  <?php foreach($kabkota as $kota): ?>
                  <option  <?php echo $profil->kota_domisili == $kota->id ? 'selected' : ''; ?> value="<?=$kota->id?>"><?=$kota->name?></option>
                  <?php endforeach;?>
                  </select>
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                     Pendidikan Terakhir
                  
                  <select class="form-control" name="pendidikan" id="pendidikan" required >
                  
                  <?php foreach($pendidikan as $pend): ?>
                  <option <?php echo $profil->pendidikan_terakhir == $pend->id ? 'selected' : ''; ?> value="<?=$pend->id?>"><?=$pend->pendidikan?></option>
                  <?php endforeach;?>
                  </select>
                  <div class="validate"></div>
                </div>
              </div>


              <div class="form-row">
                
                <div class="col-md-12 form-group">
                    Alamat
                    <textarea class="form-control" name="alamat" id="alamat" placeholder="Tuliskan Alamat Lengkap Domisili Anda"><?=$profil->alamat_lengkap?></textarea>
                  <!-- <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" data-rule="required" data-msg="Tidak Boleh Kosong" /> -->
                  <div class="validate"></div>
                </div>
              </div>

              <div class="form-row">
                <div class="col-md-6 form-group">
                     Jenis Kelamin
                  
                  <select class="form-control" name="gender" id="gender" required >
                 
                  <option <?php echo $profil->gender == 'L' ? 'selected' : ''; ?> value="L">Laki-Laki</option>
                  <option <?php echo $profil->gender == 'P' ? 'selected' : ''; ?>value="P">Perempuan</option>
                  </select>
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                     Golongan Darah
                  
                  <select class="form-control" name="golongan_darah" id="golongan_darah" required >
                  
                  <option <?php echo $profil->golongan_darah == 'A' ? 'selected' : ''; ?> value="A">A</option>
                  <option <?php echo $profil->golongan_darah == 'B' ? 'selected' : ''; ?> value="B">B</option>
                  <option <?php echo $profil->golongan_darah == 'AB' ? 'selected' : ''; ?> value="AB">AB</option>
                  <option <?php echo $profil->golongan_darah == 'O' ? 'selected' : ''; ?> value="O">O</option>
                  </select>
                  <div class="validate"></div>
                </div>
              </div>

              <br/>
              <img width="30%" src="<?=base_url($profil->foto)?>">

              <div class="form-row">
                
                <div class="col-md-12 form-group">
                     Foto Anda
                    <!-- <textarea class="form-control" name="file" id="file" placeholder="Tuliskan Alamat Lengkap Domisili Anda"></textarea> -->
                  <input type="file" class="form-control h-auto" name="foto" id="uploadImage" required  data-msg="Tidak Boleh Kosong" />
                  <div id="result" class="uploadPreview">
                  <div class="validate"></div>
                </div>
              </div>

              </div>

              

              
              <!-- <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                <div class="validate"></div>
              </div> -->
              
              <!-- <div class="text-center"><button class="btn btn-success" type="submit">Update</button></div> -->
              <?php echo form_close(); ?>

          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

    <?php
  $this->load->view('frontend/a_footer_acc');
  ?>

<script>
  window.onload = function() {
  if (window.File && window.FileList && window.FileReader) {
    var filesInput = document.getElementById("uploadImage");
    filesInput.addEventListener("change", function(event) {
      var files = event.target.files;
      var output = document.getElementById("result");
      for (var i = 0; i < files.length; i++) {
        var file = files[i];
        if (!file.type.match('image'))
          continue;
        var picReader = new FileReader();
        picReader.addEventListener("load", function(event) {
          var picFile = event.target;
          var div = document.createElement("div");
          div.innerHTML = "<img width='30%' class='thumbnail' src='" + picFile.result + "'" +
            "title='" + picFile.name + "'/>";
          output.insertBefore(div, null);
        });        
        picReader.readAsDataURL(file);
      }

    });
  }
}
</script>