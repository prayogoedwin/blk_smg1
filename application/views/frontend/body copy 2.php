  <!-- ======= Hero Section ======= -->
  <section id="hero" style="height:400px;">
    <div class="hero-container">
      <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

        <!-- <ol class="carousel-indicators" id="hero-carousel-indicators"></ol> -->

        <div class="carousel-inner" >

          <?php foreach($slider as $sld): 
            
          
          if($sld->id == $slider_max->x){
            $act = 'active';
          }else{
            $act = '';
          }
          ?>
          <!-- Slide 1 -->
          <div class="carousel-item <?=$act?>" >
          
             
              <img src="<?=base_url($sld->file)?>" class="img-responsive" style="width:100%">
              
              <!-- <a href="" class="btn btn-get-started">Read More</a> -->
             
           
          </div>

          <?php endforeach; ?>

        </div>

        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon icofont-rounded-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon icofont-rounded-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
  </section><!-- End Hero -->


    <main id="main">

    <!-- ======= Featured Section ======= -->
    <section id="pelatihan" class="featured">
      <div class="container">
   

        <div class="section-title" data-aos="fade-up">
          <h2>Daftar Pelatihan 2021</h2>
        </div>

        <div class="row">
        <?php foreach($pelatihan as $pel): ?>
          <div class="col-lg-4">
            <div class="icon-box">
              <!-- <i class="icofont-learn"></i> -->
              <h3><a href=""><?=$pel->nama_pelatihan?></a></h3>
              <p><?=$this->formatter->getDateMonthFormatUser($pel->tgl_pembukaan_pendaftaran)?> - <?=$this->formatter->getDateMonthFormatUser($pel->tgl_penutupan_pendaftaran)?></p>
              <div class="expanded-text">
                <span class="short-name">Lihat Detail</span><br/>
                <span class="longer-name"><?=$pel->deskripsi?></span>
               <?php $a = $this->session->userdata('id_pengguna');
                if( $a == ''){ ?>
                  <a href="<?=base_url('publik/login')?>" type="button" class="btn" style="background-color:#fff; float: right; border-color:#D00000;">Login Untuk Mendaftar Pelatihan</a>
                <?php }else{ ?> 
                  <a href="<?=base_url('publik/daftar_pelatihan/'.$pel->id)?>" type="button" class="btn" style="background-color:#fff; float: right; border-color:#D00000;">Daftar Pelatihan</a>
                  <?php }?> 
                </div>
                <br/>
                

                
            </div>
            <br/>
          </div>
         
          <?php endforeach; ?>
          
        </div>
        <br/>
       

      </div>
    </section><!-- End Featured Section -->


    <section id="karir" class="team" style="background-color: #D00000; color:#fff">
      <div class="container">

        <br/>
        <br/>
        <br/>

        <div class="section-title" data-aos="fade-up">
          <h2>Karir</h2>
        </div>

        <div class="row">
         
          <table id="example2" class="table table-bordered table-striped" style="color:#fff">
            <thead>
            <tr style="text-align: center;">
              <th>No</th>
              <th>Lowongan</th>
              <th>Kab/Kota</th>
              <th>Tanggal Tutup Lowongan</th>
            </tr>
            </thead>
            <tbody>
           
            <?php
                $url = "https://bursakerja.jatengprov.go.id/api/infografis/lowongan/";
                
                // var_dump($url);
                $result = file_get_contents($url);
                
                // var_dump($result);
                
                $a = json_decode($result, true);
                $no = 0;
    
                foreach( $a['data'] as $all):
                $no++;
                ?>

                   <td><?=$no?></td>
                   <td><?=$all['loker']?>  (<?=$all['perusahaan']?>)</td>
                   <td><?=$all['kota']?></td>
                   <td><?=date('d-m-Y', strtotime($all['tutup']))?></td>
                   
                </tr>
                <?php endforeach;?>
               
               
            
     

          </tbody>
          </table>
          <p>Data karir di atas diambil langsung dari website <a target="BLANK" style="color:black" href="http://bursakerja.jatengprov.go.id/">bursakerja.jatengprov.go.id</a></p>

        </div>

      </div>
    </section><!-- End Team Section -->


    <section id="alumni2" class="team" hidden>
      <div class="container">

        <br/>
        <br/>
        <br/>

        <div class="section-title" data-aos="fade-up">
          <h2>Alumni</h2>
        </div>

        <div class="row">

        <?php foreach($testimonial as $testi): ?>
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch" >
            <div class="member">
              <img src="<?=base_url($testi->foto)?>" alt="">
              <h4 style="color:#D00000"><?=$testi->nama?></h4>
              <!-- <span>CTO</span> -->
              <p>
              Alumni <?=$testi->nama_pelatihan?>
              <?=$testi->tgl_kelas_mulai?>
              </p>
              <!-- <div class="social">
                <a href=""><i class="icofont-twitter"></i></a>
                <a href=""><i class="icofont-facebook"></i></a>
                <a href=""><i class="icofont-instagram"></i></a>
                <a href=""><i class="icofont-linkedin"></i></a>
              </div> -->
            </div>
          </div>
          <?php endforeach; ?>

          

        </div>

      </div>
    </section><!-- End Team Section -->

    <section id="alumni" class="team" >
      <div class="container">

        <br/>
        <br/>
        <br/>

        <div class="section-title" data-aos="fade-up">
          <h2>Alumni</h2>
        </div>

        <div class="row">
<style>
.item {
    width: 120px;
    min-height: 120px;
    max-height: auto;
    /* float: left; */
    margin: 3px;
    padding: 3px;
}
</style>
        <?php foreach($testimonial_ori as $testi_ori): ?>
          
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch" >
          
            <div class="member">
            <a target="BLANK" href="<?=base_url('publik/detail_testimonial/'.$testi_ori->id.'/'.$testi_ori->id_pelatihan.'/'.$testi_ori->kodeembed)?>">
              <img style="max-width: 100%;  height: auto; border-radius: 50%;" class="img-responsive item" src="<?=base_url($testi_ori->foto)?>" alt="">
              <h4 style="color:#D00000"><?=$testi_ori->nama?></h4>
              <!-- <span>CTO</span> -->
              <p>
              Alumni <?=$testi_ori->nama_pelatihan?>
              <?=$testi_ori->tgl_kelas_mulai?>
              </p>
              <!-- <div class="social">
                <a href=""><i class="icofont-twitter"></i></a>
                <a href=""><i class="icofont-facebook"></i></a>
                <a href=""><i class="icofont-instagram"></i></a>
                <a href=""><i class="icofont-linkedin"></i></a>
              </div> -->
              </a>
            </div>
           
          </div>
         
          <?php endforeach; ?>

          

        </div>

      </div>
    </section><!-- End Team Section -->


    <section id="faq" class="team" style="background-color: #D00000;">
      <div class="container">

        <br/>
        <br/>
        <br/>

        <div class="section-title" style="color:#FFF" data-aos="fade-up">
          <h2>Tanya Jawab Umum</h2>
        </div>

        <div class="row">

          <div class="card card-lg" style="width: 100%;">
            
            <div class="accordion accordion-type-2 accordion-flush" id="accordion_2">
           

            <?php foreach($tanyajawab as $faq): ?>
                <div class="card">
                    <div class="card-header d-flex justify-content-between <?php echo $maxfaq->id == $faq->id ? "activestate" : " ";?>">
                        <a class="<?php echo $maxfaq->id == $faq->id ? "" : "collapsed";?>" role="button" data-toggle="collapse" href="#collapse_<?=$faq->id?>i" aria-expanded="<?php echo $maxfaq->id == $faq->id ? "true" : "false";?>"><?=$faq->pertanyaan?></a>
                    </div>
                    <div id="collapse_<?=$faq->id?>i" class="collapse <?php echo $maxfaq->id == $faq->id ? "show" : "";?>" data-parent="#accordion_2" role="<?php echo $maxfaq->id == $faq->id ? "tabpanel" : "";?>">
                        <div class="card-body pa-15"><?=$faq->jawaban?></div>
                    </div>
                </div>
            <?php endforeach; ?>

                

                
            </div>

        </div>
         

        </div>

      </div>
    </section><!-- End Team Section -->

     <!-- ======= Contact Section ======= -->
     <section id="kontak" class="contact">
      <div class="container" data-aos="fade-up">
          <br/>
          <br/>
          <br/>

        <div class="section-title">
          <h2>Kontak</h2>
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>

        <div>
       
          <iframe style="border:0; width: 100%; height: 270px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126719.34725434512!2d110.28852440732825!3d-7.011681663168971!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e70f4b81d4aeb5f%3A0xe0f247bae736cc1c!2sJl.%20Brotojoyo%20Bar.%2C%20Panggung%20Kidul%2C%20Kec.%20Semarang%20Utara%2C%20Kota%20Semarang%2C%20Jawa%20Tengah%2050178!5e0!3m2!1sid!2sid!4v1615902891225!5m2!1sid!2sid" frameborder="0" allowfullscreen></iframe>
        </div>

        <div class="row mt-5">

          <div class="col-lg-4">
            <div class="info">
              <div class="address">
                <i class="icofont-google-map"></i>
                <h4>Alamat:</h4>
                <p>Jl. Brotojoyo Bar., Panggung Kidul, Kec. Semarang Utara, Kota Semarang, Jawa Tengah 50178</p>
              </div>

              <div class="email">
                <i class="icofont-phone"></i>
                <h4>Telepon:</h4>
                <p>(024) 3548396</p>
              </div>

              <div class="phone">
                <i class="icofont-envelope"></i>
                <h4>Email:</h4>
                <p>blksemarang1@gmail.com</p>
              </div>

            </div>

          </div>

          <div class="col-lg-8 mt-5 mt-lg-0">

            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="form-row">
                <div class="col-md-6 form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                <div class="validate"></div>
              </div>
              <div class="mb-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>

          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  


  <?php
  $this->load->view('frontend/a_footer');
  ?>