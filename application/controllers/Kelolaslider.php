<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kelolaslider extends CI_Controller
{



    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('M_kelolaslider');
    }

    public function index()
	{
        $data['alldata'] = $this->M_kelolaslider->get_all();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/slider/index',$data);
	}

    public function tambah()
	{
        $data['persyaratan'] = $this->db->query("SELECT * FROM si_syarat")->result();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/slider/form_tambah',$data);
	}

    public function aksi_tambah()
	{
       $user = $this->session->userdata('id_pengguna');
       $datenow = date('Y-m-d H:i:s');

       $filename = null;
			
        $config['upload_path'] = './upload/'; //path folder
        $config['allowed_types'] = 'jpg|jpeg|png|pdf'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('foto') || empty($_FILES['foto']['name'])) {
            if (!empty($_FILES['foto']['name'])) {
                $upload = $this->upload->data();
                $filename = '/upload/'.$upload['file_name'];
            }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('notice', 'Gagal');
                $this->session->set_flashdata('message', 'Upload Foto Gagal');
                redirect(site_url('kelolaslider/tambah'));
            }
        } else {
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('notice', 'Gagal');
                $this->session->set_flashdata('message', 'Upload Foto Gagal X');
                redirect(site_url('kelolaslider/tambah'));
        }

       $data = array(
           'file'   => $filename,
           'created_by' => $user,
           'created_at' => $datenow
       );
       $insert = $this->db->insert('si_slider', $data);

       if($insert){
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('notice', 'Success');
            $this->session->set_flashdata('message', 'Tambah data Berhasil');
            redirect(site_url('kelolaslider'));
        }else{
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('notice', 'Gagal');
            $this->session->set_flashdata('message', 'Tambah data gagal');
            redirect(site_url('kelolaslider/tambah'));
        }
       
	}

    public function hapus($id)
    {
        $row = $this->M_kelolaslider->soft_del($id);
        if ($row) { 
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Hapus data berhasil');
            redirect(site_url('kelolaslider'));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Hapus data gagal ');
            redirect(site_url('kelolaslider'));
        }
    }

}?>