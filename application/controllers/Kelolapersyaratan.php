<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kelolapersyaratan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('M_kelolapersyaratan');
    }

    public function index()
	{
        $data['alldata'] = $this->M_kelolapersyaratan->get_all();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/persyaratan/index',$data);
	}

    public function tambah()
	{
        $data['persyaratan'] = $this->db->query("SELECT * FROM si_syarat")->result();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/persyaratan/form_tambah',$data);
	}

    public function aksi_tambah()
	{
       $user = $this->session->userdata('id_pengguna');
       $datenow = date('Y-m-d H:i:s');
       $deskripsi = $this->input->post('deskripsi');

       $data = array(
           'deskripsi' => $deskripsi,
           'as'     => 'selected',
           'created_by' => $user,
           'created_at' => $datenow
       );
       $insert = $this->db->insert('si_syarat', $data);

       if($insert){
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('notice', 'Success');
            $this->session->set_flashdata('message', 'Tambah data Berhasil');
            redirect(site_url('kelolapersyaratan'));
        }else{
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('notice', 'Gagal');
            $this->session->set_flashdata('message', 'Tambah data gagal');
            redirect(site_url('kelolapersyaratan'));
        }
       
	}

    public function hapus($id)
    {
        $row = $this->M_kelolapersyaratan->soft_del($id);
        if ($row) { 
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Hapus data berhasil');
            redirect(site_url('kelolapersyaratan'));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Hapus data gagal ');
            redirect(site_url('kelolapersyaratan'));
        }
    }

}?>