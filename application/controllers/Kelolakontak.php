<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kelolakontak extends CI_Controller
{



    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('M_kelolakontak');
    }

    public function index()
	{
        $data['alldata'] = $this->M_kelolakontak->get_all();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kontak/index',$data);
	}

}?>