<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publik extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
    {
        parent::__construct();
        $this->load->model('M_kelolapelatihan');
		$this->load->model('M_kelolafaq');
		$this->load->model('M_kelolaslider');
		$this->load->model('M_welcome');
		$this->dbk = $this->load->database('dbkorea', TRUE);
    }

	public function index()
	{
		$data['pelatihan'] = $this->M_kelolapelatihan->get_all_show_limit(6);
		$data['slider'] = $this->M_kelolaslider->get_all();
		$data['slider_max'] = $this->M_kelolaslider->max_id();
		$data['tanyajawab'] = $this->M_kelolafaq->get_all();
		$data['maxfaq'] = $this->db->query("SELECT min(id) as id from si_faq")->row();
		// $data['testimonial'] = $this->db->query("SELECT a.judul_testimoni, b.nama, c.nama_pelatihan, c.tgl_kelas_mulai, b.foto
		// FROM si_testimonial a 
		// INNER JOIN si_user b ON b.id_pengguna = a.id_user	
		// LEFT JOIN si_pelatihan c ON c.id = a.id_pelatihan	
		// ")->result();

// 		$data['testimonial_ori'] = $this->db->query("SELECT c.id as id_pelatihan, a.id, a.testimoni, a.video, SUBSTRING(a.video, 33) as kodeembed, b.nama, c.nama_pelatihan, c.tgl_kelas_mulai, b.foto
// 		FROM si_pelatihan_pendaftaran a 
// 		INNER JOIN si_user b ON b.id_pengguna = a.id_user	
// 		LEFT JOIN si_pelatihan c ON c.id = a.id_pelatihan")->result();
        $data['testimonial_ori'] = $this->db->query("SELECT c.id as id_pelatihan, a.id, a.testimoni, a.video, SUBSTRING(a.video, 33) as kodeembed, b.nama, c.nama_pelatihan, c.tgl_kelas_mulai, b.foto
		FROM si_pelatihan_pendaftaran a 
		INNER JOIN si_user b ON b.id_pengguna = a.id_user	
		INNER JOIN si_pelatihan c ON c.id = a.id_pelatihan ORDER BY id_pelatihan LIMIT 9")->result();
        $this->load->view('frontend/a_head');
		$this->load->view('frontend/a_header');
        $this->load->view('frontend/body',$data);

	}

	public function detail_testimonial($id, $pelatihan)
	{
				$data['pelatihan'] = $this->db->query("SELECT * FROM si_pelatihan WHERE id = '$pelatihan' AND deleted_at IS NULL AND `status` = 1 ")->row();
				// $data['pendaftaran'] = $this->db->query("SELECT * FROM si_pelatihan_pendaftaran WHERE id = '$id' AND deleted_at IS NULL")->row();
				$data['pendaftaran'] = $this->db->query("SELECT c.id as id_pelatihan, a.video, a.id, a.testimoni, b.nama, c.nama_pelatihan, c.tgl_kelas_mulai, b.foto
				FROM si_pelatihan_pendaftaran a 
				INNER JOIN si_user b ON b.id_pengguna = a.id_user	
				LEFT JOIN si_pelatihan c ON c.id = a.id_pelatihan	
				WHERE a.id = $id 
				")->row();
				$this->load->view('frontend/a_head');
				$this->load->view('frontend/a_header_acc');
				$this->load->view('frontend/detail_testimoni', $data);
	}

    public function register()
	{
		$data['kabkota'] = $this->db->query("SELECT * FROM si_kota WHERE province_id = 33")->result();
		$data['pendidikan'] = $this->db->query("SELECT * FROM si_pendidikan")->result();
        $this->load->view('frontend/a_head');
		$this->load->view('frontend/a_header_acc');
        $this->load->view('frontend/register',$data);

	}
	
    public function aksi_register()
	{
       $datenow = date('Y-m-d H:i:s');
       $nama = $this->input->post('name', TRUE);
       $nik = $this->input->post('nik', TRUE);
       $email = $this->input->post('email', TRUE);
       $password = $this->input->post('password', TRUE);
       $wa = $this->input->post('wa', TRUE);
       $hp = $this->input->post('hp', TRUE);
       $tempat = $this->input->post('tempat', TRUE);
       $tanggal = $this->input->post('tanggal', TRUE);
       $domisili = $this->input->post('domisili', TRUE);
	   $pendidikan = $this->input->post('pendidikan', TRUE);

	   $alamat = $this->input->post('alamat', TRUE);

	   $gender = $this->input->post('gender', TRUE);
	   $golongan_darah = $this->input->post('golongan_darah', TRUE);
	   $filename=NULL;

	   	$config['upload_path'] = './upload/user/'; //path folder
		$config['allowed_types'] = 'jpg|jpeg|png'; //type yang dapat diakses bisa anda sesuaikan
		$config['encrypt_name'] = TRUE; //nama yang terupload nantinya
		$this->load->library('upload', $config);

		if ($this->upload->do_upload('foto') || empty($_FILES['foto']['name'])) {
			if (!empty($_FILES['foto']['name'])) {
				$upload = $this->upload->data();
				$filename = '/upload/user/'.$upload['file_name'];
			}else{
				$this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('notice', 'Gagal');
                $this->session->set_flashdata('message', 'Regsistrasi gagal, Upload Foto Gagal');
                redirect(site_url('publik/register'));
			}
		} else {

				$this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('notice', 'Gagal');
                $this->session->set_flashdata('message', 'Regsistrasi gagal, Upload Foto Gagal X');
                redirect(site_url('publik/register'));
			// $filename = '-';
			// echo 'Gagal Upload';
			// if (($_FILES['file'.$q->id]['size']/1000)> 500) {
			// 	$this->session->set_flashdata('info', 'danger');
            //     $this->session->set_flashdata('notice', 'Gagal');
            //     $this->session->set_flashdata('message', 'Regsistrasi gagal, Ukuran Foto Lebih dari 500kb');
            //     redirect(site_url('publik/register')); 
			// } else {
			// 	$this->session->set_flashdata('info', 'danger');
            //     $this->session->set_flashdata('notice', 'Gagal');
            //     $this->session->set_flashdata('message', 'Regsistrasi gagal - Upload Foto Bermasalah');
            //     redirect(site_url('publik/register'));
			// }
		}

       $data = array(
		   'username' => $email,
		   'password' => sha1($password),
		   'role'	  => 2,
           'nama' 	  => $nama,
           'nik' => $nik,
           'tempat_lahir' => $tempat,
           'tgl_lahir' => $tanggal,
           'wa' => $wa,
           'hp' => $hp,
           'kota_domisili' => $domisili,
		   'pendidikan_terakhir' => $pendidikan,
           'alamat_lengkap' => $alamat,
		   'gender' => $gender,
		   'golongan_darah' => $golongan_darah,
		   'foto' => $filename,
           'created_at' => $datenow
       );
       $insert = $this->db->insert('si_user', $data);
       $lastid = $this->db->insert_id();

       if($insert){
            $password_new = password_hash(($password), PASSWORD_DEFAULT);
            // 			$endpoint = 'https://blksmg1.disnakertrans.jatengprov.go.id/korea/api.php';
            // id_blk, nik, nama, email, password, role
			$params = array(
				 // 'add'  => 1,
				'id_blk'   => $lastid,
				'nik'  	   => $nik,
				'nama'	   => $nama,
				'email'	   => $email,
				'role'     => 'peserta',
				'password' => $password_new,
				
			);
            // $result = ClientPost($endpoint, $params);
			// $object = json_decode($result, TRUE);
			// echo $result;
			
			$insert_k = $this->dbk->insert('peserta', $params);
// 			echo json_encode($insert_k);
			if ($insert_k) {
				$this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('notice', 'Berhasil');
                $this->session->set_flashdata('message', 'Regsistrasi berhasil');
                redirect(site_url('publik/login'));
				// redirect('https://blksmg1.disnakertrans.jatengprov.go.id/korea/');
			} else {
				$this->db->where('id', $lastid);
                $this->db->delete('si_user'); 
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('notice', 'Gagal');
                $this->session->set_flashdata('message', 'Regsistrasi gagal A');
                redirect(site_url('publik/register'));
			}
        }else{
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('notice', 'Gagal');
            $this->session->set_flashdata('message', 'Regsistrasi gagal B');
            redirect(site_url('publik/register'));
        }
       
	}

	public function aksi_register_old()
	{
       $datenow = date('Y-m-d H:i:s');
       $nama = $this->input->post('name', TRUE);
       $nik = $this->input->post('nik', TRUE);
       $email = $this->input->post('email', TRUE);
       $password = $this->input->post('password', TRUE);
       $wa = $this->input->post('wa', TRUE);
       $hp = $this->input->post('hp', TRUE);
       $tempat = $this->input->post('tempat', TRUE);
       $tanggal = $this->input->post('tanggal', TRUE);
       $domisili = $this->input->post('domisili', TRUE);
	   $pendidikan = $this->input->post('pendidikan', TRUE);

	   $alamat = $this->input->post('alamat', TRUE);

	   $gender = $this->input->post('gender', TRUE);
	   $golongan_darah = $this->input->post('golongan_darah', TRUE);
	   $filename=NULL;

	   	$config['upload_path'] = './upload/user/'; //path folder
		$config['allowed_types'] = 'jpg|jpeg|png'; //type yang dapat diakses bisa anda sesuaikan
		$config['encrypt_name'] = TRUE; //nama yang terupload nantinya
		$this->load->library('upload', $config);

		if ($this->upload->do_upload('foto') || empty($_FILES['foto']['name'])) {
			if (!empty($_FILES['foto']['name'])) {
				$upload = $this->upload->data();
				$filename = '/upload/user/'.$upload['file_name'];
			}else{
				$this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('notice', 'Gagal');
                $this->session->set_flashdata('message', 'Regsistrasi gagal, Upload Foto Gagal');
                redirect(site_url('publik/register'));
			}
		} else {

				$this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('notice', 'Gagal');
                $this->session->set_flashdata('message', 'Regsistrasi gagal, Upload Foto Gagal X');
                redirect(site_url('publik/register'));
			// $filename = '-';
			// echo 'Gagal Upload';
			// if (($_FILES['file'.$q->id]['size']/1000)> 500) {
			// 	$this->session->set_flashdata('info', 'danger');
            //     $this->session->set_flashdata('notice', 'Gagal');
            //     $this->session->set_flashdata('message', 'Regsistrasi gagal, Ukuran Foto Lebih dari 500kb');
            //     redirect(site_url('publik/register')); 
			// } else {
			// 	$this->session->set_flashdata('info', 'danger');
            //     $this->session->set_flashdata('notice', 'Gagal');
            //     $this->session->set_flashdata('message', 'Regsistrasi gagal - Upload Foto Bermasalah');
            //     redirect(site_url('publik/register'));
			// }
		}

       $data = array(
		   'username' => $email,
		   'password' => sha1($password),
		   'role'	  => 2,
           'nama' 	  => $nama,
           'nik' => $nik,
           'tempat_lahir' => $tempat,
           'tgl_lahir' => $tanggal,
           'wa' => $wa,
           'hp' => $hp,
           'kota_domisili' => $domisili,
		   'pendidikan_terakhir' => $pendidikan,
           'alamat_lengkap' => $alamat,
		   'gender' => $gender,
		   'golongan_darah' => $golongan_darah,
		   'foto' => $filename,
           'created_at' => $datenow
       );
       $insert = $this->db->insert('si_user', $data);
       $lastid = $this->db->insert_id();

       if($insert){
// $password = password_hash($xss($_POST["password"]), PASSWORD_DEFAULT);
			$endpoint = 'https://blksmg1.disnakertrans.jatengprov.go.id/korea/api.php';
			$params = array(
				'add'      => 1,
				'id_blk'   => $lastid,
				'nik'  	   => $nik,
				'nama'	   => $nama,
				'email'	   => $email,
				'password' => $password
			);
			$result = ClientPost($endpoint, $params);
			// $object = json_decode($result, TRUE);
			// echo $result;
			if ($result == 'berhasil') {
				$this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('notice', 'Berhasil');
                $this->session->set_flashdata('message', 'Regsistrasi berhasil');
                redirect(site_url('publik/login'));
				// redirect('https://blksmg1.disnakertrans.jatengprov.go.id/korea/');
			} else {
				$this->db->where('id', $lastid);
                $this->db->delete('si_user'); 
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('notice', 'Gagal');
                $this->session->set_flashdata('message', 'Regsistrasi gagal A');
                redirect(site_url('publik/register'));
			}
        }else{
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('notice', 'Gagal');
            $this->session->set_flashdata('message', 'Regsistrasi gagal B');
            redirect(site_url('publik/register'));
        }
       
	}

    public function login()
	{
		
          $a = $this->session->userdata('id_pengguna');
          if( $a != ''){ 
			redirect(site_url('publik/profil'));
          }else{ 
			$this->load->view('frontend/a_head');
			$this->load->view('frontend/a_header_acc');
			$this->load->view('frontend/login');
		  }
	}

	public function profil()
	{
		$a = $this->session->userdata('id_pengguna');
		$data['profil'] = $this->db->query("SELECT * FROM si_user WHERE id_pengguna = '$a' AND deleted_at IS NULL")->row();
		$data['kabkota'] = $this->db->query("SELECT * FROM si_kota WHERE province_id = 33")->result();
		$data['pendidikan'] = $this->db->query("SELECT * FROM si_pendidikan")->result();
        $this->load->view('frontend/a_head');
		$this->load->view('frontend/a_header_acc');
        $this->load->view('frontend/profil', $data);
	}

	public function dashboard()
	{
		$a = $this->session->userdata('id_pengguna');
		$data['pelatihan'] = $this->M_kelolapelatihan->get_all_show_limit(12);
		$data['profil'] = $this->db->query("SELECT * FROM si_user WHERE id_pengguna = '$a' AND deleted_at IS NULL")->row();
		$data['kabkota'] = $this->db->query("SELECT * FROM si_kota WHERE province_id = 33")->result();
        $this->load->view('frontend/a_head');
		$this->load->view('frontend/a_header_acc');
        $this->load->view('frontend/dashboard', $data);
	}

	public function daftar_pelatihan($id)
	{
		$a = $this->session->userdata('id_pengguna');
          if( $a == ''){ 
			redirect(site_url('publik'));
          }else{ 

				$data['pelatihan'] = $this->db->query("SELECT * FROM si_pelatihan WHERE id = '$id' AND deleted_at IS NULL AND `status` = 1 ")->row();
				$data['quesioner_umum'] = $this->M_welcome->pelatihan_quesioner($id, 1);	
				$data['quesioner_umum_s'] = $this->M_welcome->pelatihan_quesioner_show($id, 1);	
				$data['quesioner_demografi'] = $this->M_welcome->pelatihan_quesioner($id, 2);
				$data['quesioner_demografi_s'] = $this->M_welcome->pelatihan_quesioner_show($id, 2);	
				$data['quesioner_minat'] = $this->M_welcome->pelatihan_quesioner($id, 3);
				$data['quesioner_minat_s'] = $this->M_welcome->pelatihan_quesioner_show($id, 3);
				$data['quesioner_khusus'] = $this->M_welcome->pelatihan_quesioner($id, 4);
				$data['quesioner_khusus_s'] = $this->M_welcome->pelatihan_quesioner_show($id, 4);
				$data['quesioner_upload'] = $this->M_welcome->pelatihan_quesioner($id, 5);
				$data['quesioner_upload_s'] = $this->M_welcome->pelatihan_quesioner_show($id, 5);
				$this->load->view('frontend/a_head');
				$this->load->view('frontend/a_header_acc');
				$this->load->view('frontend/detail_pelatihan', $data);
		  }
	}

	public function sukses_daftar_pelatihan($id)
	{
		$a = $this->session->userdata('id_pengguna');
          if( $a == ''){ 
			redirect(site_url('publik'));
          }else{ 

				$data['pelatihan'] = $this->db->query("SELECT * FROM si_pelatihan WHERE id = '$id' AND deleted_at IS NULL AND `status` = 1 ")->row();
				$data['quesioner_umum'] = $this->M_welcome->pelatihan_quesioner($id, 1);	
				$data['quesioner_umum_s'] = $this->M_welcome->pelatihan_quesioner_show($id, 1);	
				$data['quesioner_demografi'] = $this->M_welcome->pelatihan_quesioner($id, 2);
				$data['quesioner_demografi_s'] = $this->M_welcome->pelatihan_quesioner_show($id, 2);	
				$data['quesioner_minat'] = $this->M_welcome->pelatihan_quesioner($id, 3);
				$data['quesioner_minat_s'] = $this->M_welcome->pelatihan_quesioner_show($id, 3);
				$data['quesioner_khusus'] = $this->M_welcome->pelatihan_quesioner($id, 4);
				$data['quesioner_khusus_s'] = $this->M_welcome->pelatihan_quesioner_show($id, 4);
				$data['quesioner_upload'] = $this->M_welcome->pelatihan_quesioner($id, 5);
				$data['quesioner_upload_s'] = $this->M_welcome->pelatihan_quesioner_show($id, 5);
				$this->load->view('frontend/a_head');
				$this->load->view('frontend/a_header_acc');
				$this->load->view('frontend/sukses_detail_pelatihan', $data);
		  }
	}

	public function aksi_register_pelatihan_(){
		$datenow = date('Y-m-d H:i:s');
		$id_user = $this->session->userdata('id_pengguna');
		$id_pelatihan = $this->input->post('id_pelatihan');

		

			$quesioner = $this->M_welcome->pelatihan_quesioner_full($id_pelatihan);	
			$data = array();
			foreach($quesioner as $q){
				if($q->tipe_quesioner == 4 && $q->tipe_jawaban == 3){
					$multi = array();
					$jwb = $this->input->post('jawaban'.$q->id);
					foreach($jwb as $key=>$value) {
						$multi[]  = array(
								$jwb[$key], 
							);
					}
					echo json_encode($multi);
				}
				
			}

	}

	public function aksi_register_pelatihan(){
		$datenow = date('Y-m-d H:i:s');
		$id_user = $this->session->userdata('id_pengguna');
		$id_pelatihan = $this->input->post('id_pelatihan');

		$data = array(
			'id_user' 		=> $id_user,
			'id_pelatihan' 	=> $id_pelatihan,
			'status'	  	=> 0,
			'created_at' 	=> $datenow
		);
		$insert_pelatihan = $this->db->insert('si_pelatihan_pendaftaran', $data);
		$id_pelatihan_pendaftaran = $this->db->insert_id();
		// $id_pelatihan_pendaftaran = 7;

		$insert_pelatihan = true;

		if($insert_pelatihan){
			$quesioner_a = $this->M_welcome->pelatihan_quesioner_full($id_pelatihan);	
			$filename = null;
			
			$config['upload_path'] = './file/'; //path folder
			$config['allowed_types'] = 'jpg|jpeg|png|pdf'; //type yang dapat diakses bisa anda sesuaikan
			$config['encrypt_name'] = TRUE; //nama yang terupload nantinya
			$this->load->library('upload', $config);

			$quesioner = $this->M_welcome->pelatihan_quesioner_full($id_pelatihan);	
			$data = array();
			foreach($quesioner as $q){

				if($q->tipe_quesioner == 5 && $q->quesioner == 1){
					if ($this->upload->do_upload('file'.$q->id) || empty($_FILES['file'.$q->id]['name'])) {
						if (!empty($_FILES['file'.$q->id]['name'])) {
							$upload = $this->upload->data();
							rename($config['upload_path'].$upload['file_name'],$config['upload_path'].$id_pelatihan_pendaftaran.'_'.$q->id_pelatihan_quesioner.'_'.$q->id.'_'.$upload['file_name']);
							// $filename = $upload['file_name'];
							$filename = $id_pelatihan_pendaftaran.'_'.$q->id_pelatihan_quesioner.'_'.$q->id.'_'.$upload['file_name'];
						}
					} else {
						$filename = '-';
						// echo 'Gagal Upload';
						// if (($_FILES['file'.$q->id]['size']/1000)> 500) {
						// 	// echo 'gagal';
						// 	// $this->response([
						// 	// 	'status' => FALSE,
						// 	// 	'message' => 'Upload Gagal, Ukuran File Melebihi 500KB',
						// 	// 	'data'    => $data
						// 	// ], REST_Controller::HTTP_OK);  
						// } else {
						// 	// echo 'berhasil';
						// 	// $this->response([
						// 	// 	'status' => FALSE,
						// 	// 	'message' => 'Upload '.$jenis_file.' Gagal',
						// 	// 	'data'    => $data
						// 	// ], REST_Controller::HTTP_OK); 
						// }
					}
				}

				if($q->tipe_quesioner == 5 && $q->quesioner == 1){
					$data[] = array(
						'id_pelatihan_pendaftaran' => $id_pelatihan_pendaftaran,
						'id_pelatihan_quesioner'   => $q->id_pelatihan_quesioner,
						'id_quesioner'			   => $q->id_quesioner,
						'jawaban'			 	   => $filename
					);

				}elseif($q->tipe_quesioner == 4 && $q->tipe_jawaban == 3){
					$multi = array();
					$jwb = $this->input->post('jawaban'.$q->id);
					foreach($jwb as $key=>$value) {
						$multi[]  = array(
								$jwb[$key], 
							);
					}
					$x =  json_encode($multi);
					
					$data[] = array(
						'id_pelatihan_pendaftaran' => $id_pelatihan_pendaftaran,
						'id_pelatihan_quesioner'   => $q->id_pelatihan_quesioner,
						'id_quesioner'			   => $q->id_quesioner,
						'jawaban'			 	   => $x
					);
				}else{
					$data[] = array(
						'id_pelatihan_pendaftaran' => $id_pelatihan_pendaftaran,
						'id_pelatihan_quesioner'   => $q->id_pelatihan_quesioner,
						'id_quesioner'			   => $q->id_quesioner,
						'jawaban'			 	   => $this->input->post('jawaban'.$q->id)
					);
				}
				
			}

			// echo json_encode($data);
			$insert_quesioner = $this->db->insert_batch('si_pelatihan_pendaftaran_quesioner', $data);
			if($insert_quesioner){
				$this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('notice', 'Gagal');
				$this->session->set_flashdata('message', 'Selamat Anda Berhasil Daftar Pelatihan');
				// redirect(site_url('publik/daftar_pelatihan/'.$id_pelatihan));
				redirect(site_url('publik/sukses_daftar_pelatihan/'.$id_pelatihan));
			}else{
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('notice', 'Gagal');
				$this->session->set_flashdata('message', 'Daftar Pelatihan Gagal');
				redirect(site_url('publik/daftar_pelatihan/'.$id_pelatihan));
			}

			

		}else{
			$this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('notice', 'Gagal');
            $this->session->set_flashdata('message', 'Daftar Pelatihan Gagal');
            redirect(site_url('publik/daftar_pelatihan/'.$id_pelatihan));
		}

	}

	public function riwayat(){
		$id_user = $this->session->userdata('id_pengguna');
		$data['alldata'] = $this->M_welcome->get_riwayat_pelatihan($id_user);	
		$this->load->view('frontend/a_head');
		$this->load->view('frontend/a_header_acc');
		$this->load->view('frontend/riwayat', $data);
		
	}

	public function kirim_testimoni($id, $pelatihan)
	{
		$a = $this->session->userdata('id_pengguna');
          if( $a == ''){ 
			   redirect(site_url('publik'));
          }else{ 

				$data['pelatihan'] = $this->db->query("SELECT * FROM si_pelatihan WHERE id = '$pelatihan' AND deleted_at IS NULL AND `status` = 1 ")->row();
				$data['pendaftaran'] = $this->db->query("SELECT * FROM si_pelatihan_pendaftaran WHERE id = '$id' AND deleted_at IS NULL")->row();
				$this->load->view('frontend/a_head');
				$this->load->view('frontend/a_header_acc');
				$this->load->view('frontend/kirim_testimoni', $data);
		  }
	}

	public function aksi_kirim_testimoni(){
		$user = $this->session->userdata('id_pengguna');
		$datenow = date('Y-m-d H:i:s');
		$id_pelatihan = $this->input->post('id_pelatihan');
		$id_pendaftaran = $this->input->post('id_pendaftaran');
		$testimoni = $this->input->post('testimoni');
		$video = $this->input->post('video');

		$data = array(
			'testimoni' => $testimoni,
			'video' => $video,
			'updated_at'=>$datenow
		);
		$this->db->where('id', $id_pendaftaran);
		$update = $this->db->update('si_pelatihan_pendaftaran',$data);
		if($update){
			$this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('notice', 'Berhasil');
            $this->session->set_flashdata('message', 'Terima Kasih Sudah Mengirim Testimoni');
            redirect(site_url('publik/kirim_testimoni/'.$id_pendaftaran.'/'.$id_pelatihan));
		}else{
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('notice', 'Gagal');
            $this->session->set_flashdata('message', 'Proses Mengirim Testimoni Gagal');
			redirect(site_url('publik/kirim_testimoni/'.$id_pendaftaran.'/'.$id_pelatihan));
		}

	}

	public function aksi_pesan(){
		$x = NULL;
		$user = $this->session->userdata('id_pengguna');
		if($user == ''){
			$x = $this->input->ip_address();
		}else{
			$x = $user;
		}
		$datenow = date('Y-m-d H:i:s');
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$hp = $this->input->post('hp');
		$wa = $this->input->post('wa');
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');

		$data = array(
			'name' => $name,
			'email' => $email,
			'hp' => $hp,
			'wa' => $wa,
			'subject' => $subject,
			'message' => $message,
			'created_by'=>$x,
			'created_at'=>$datenow
		);
		$insert = $this->db->insert('si_form',$data);
		if($insert){
			$this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('notice', 'Berhasil');
            $this->session->set_flashdata('message', 'Terima Kasih, Pesan Anda sudah terkirim');
            redirect(site_url('publik/#pesan'));
		}else{
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('notice', 'Gagal');
            $this->session->set_flashdata('message', 'Proses Mengirim Pesan Gagal');
			redirect(site_url('publik/#pesan'));
		}

	}

	public function open_notif(){
		$user = $this->session->userdata('id_pengguna');
		$datenow = date('Y-m-d H:i:s');

		$data = array(
			'status' => 1,
			'updated_at'=>$datenow
		);
		$this->db->where('for_user', $user);
		$update = $this->db->update('si_notifikasi',$data);
		if($update){
            redirect(site_url('publik/riwayat'));
		}else{
            redirect(site_url('publik'));
		}

	}

    
}
?>