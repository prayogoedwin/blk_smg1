<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kelolapelatihan extends CI_Controller
{



    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('M_kelolapelatihan');
        $this->load->model('M_welcome');
    }

    public function index()
	{
        $data['alldata'] = $this->M_kelolapelatihan->get_all();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/pelatihan/index',$data);
	}

    public function tampilkan_pelatihan($pelatihan)
    {
        $data = array(
            'status' => 1
        );
        $this->db->where('id', $pelatihan);
        $edit = $this->db->update('si_pelatihan', $data);
        if ($edit) { 
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Tampilkan data berhasil');
            redirect(site_url('kelolapelatihan/'));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Tampilkan data gagal ');
            redirect(site_url('kelolapelatihan/')); 
        }
    }

    public function hidden_pelatihan($pelatihan)
    {
        $data = array(
            'status' => 0
        );
        $this->db->where('id', $pelatihan);
        $edit = $this->db->update('si_pelatihan', $data);
        if ($edit) { 
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Tampilkan data berhasil');
            redirect(site_url('kelolapelatihan/'));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Tampilkan data gagal ');
            redirect(site_url('kelolapelatihan/'));
        }
    }

    public function tampilkan_tombol_testimoni($pelatihan)
    {
        $data = array(
            'form_testimoni' => 1
        );
        $this->db->where('id', $pelatihan);
        $edit = $this->db->update('si_pelatihan', $data);
        if ($edit) { 
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Tampilkan data berhasil');
            redirect(site_url('kelolapelatihan/testimoni/'.$pelatihan));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Tampilkan data gagal ');
            redirect(site_url('kelolapelatihan/testimoni/'.$pelatihan)); 
        }
    }

    public function hidden_tombol_testimoni($pelatihan)
    {
        $data = array(
            'form_testimoni' => 0
        );
        $this->db->where('id', $pelatihan);
        $edit = $this->db->update('si_pelatihan', $data);
        if ($edit) { 
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Tampilkan data berhasil');
            redirect(site_url('kelolapelatihan/testimoni/'.$pelatihan));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Tampilkan data gagal ');
            redirect(site_url('kelolapelatihan/testimoni/'.$pelatihan)); 
        }
    }

    public function tambah()
	{
        $data['persyaratan'] = $this->db->query("SELECT * FROM si_syarat")->result();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/pelatihan/tambah',$data);
	}

    public function aksi_tambah()
	{
       $user = $this->session->userdata('id_pengguna');
       $datenow = date('Y-m-d H:i:s');
       $nama_pelatihan = $this->input->post('nama_pelatihan');
       $kuota_pendaftaran = $this->input->post('kuota_pendaftaran');
       $kuota_kelas = $this->input->post('kuota_kelas');
       $tgl_buka_p = $this->input->post('tgl_buka_p');
       $tgl_tutup_p = $this->input->post('tgl_tutup_p');
       $tgl_mulai_k = $this->input->post('tgl_mulai_k');
       $tgl_selesai_k = $this->input->post('tgl_selesai_k');
       $deskripsi = $this->input->post('deskripsi');
       $persyaratan = $this->input->post('persyaratan');

       $data = array(
           'nama_pelatihan' => $nama_pelatihan,
           'kuota_pendaftaran' => $kuota_pendaftaran,
           'kuota_kelas' => $kuota_kelas,
           'deskripsi' => $deskripsi,
           'tgl_pembukaan_pendaftaran' => $tgl_buka_p,
           'tgl_penutupan_pendaftaran' => $tgl_tutup_p,
           'tgl_kelas_mulai' => $tgl_mulai_k,
           'tgl_kelas_selesai' => $tgl_selesai_k,
           'created_by' => $user,
           'created_at' => $datenow
       );
       $insert = $this->db->insert('si_pelatihan', $data);
       $lastid = $this->db->insert_id();

       if($insert){
           
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('notice', 'Berhasil');
                $this->session->set_flashdata('message', 'Tambah data berhasil');
                redirect(site_url('kelolapelatihan'));

            // $multi = array();
            // foreach($persyaratan as $key=>$value) {
            //     $multi[]  = array(
            //             'id_pelatihan' => $lastid,
            //             'id_syarat' => $persyaratan[$key], 
            //         );
            // }
            // $multi_persyaratan = $this->db->insert_batch('si_pelatihan_syarat',$multi);
            // if($multi_persyaratan){
            //     $this->session->set_flashdata('info', 'success');
            //     $this->session->set_flashdata('notice', 'Berhasil');
            //     $this->session->set_flashdata('message', 'Tambah data berhasil');
            //     redirect(site_url('kelolapelatihan'));
            // }else{
            //     $this->db->where('id', $lastid);
            //     $this->db->delete('si_pelatihan'); 
            //     $this->session->set_flashdata('info', 'danger');
            //     $this->session->set_flashdata('notice', 'Gagal');
            //     $this->session->set_flashdata('message', 'Tambah data gagal');
            //     redirect(site_url('kelolapelatihan'));
            // }
        }else{
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('notice', 'Gagal');
            $this->session->set_flashdata('message', 'Tambah data gagal');
            redirect(site_url('kelolapelatihan'));
        }
       
	}

    public function edit($id){
        $data['persyaratan'] = $this->db->query("SELECT * FROM si_syarat")->result();
        $data['persyaratan_pelatihan'] = $this->db->query("SELECT * FROM si_pelatihan_syarat WHERE id_pelatihan = '$id'")->result();
        $data['detail'] = $this->db->query("SELECT * FROM si_pelatihan WHERE id = '$id'")->row();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/pelatihan/edit',$data);
    }

    public function aksi_edit()
	{
       $user = $this->session->userdata('id_pengguna');
       $datenow = date('Y-m-d H:i:s');
       $id = $this->input->post('id');
       $nama_pelatihan = $this->input->post('nama_pelatihan');
       $kuota_pendaftaran = $this->input->post('kuota_pendaftaran');
       $kuota_kelas = $this->input->post('kuota_kelas');
       $tgl_buka_p = $this->input->post('tgl_buka_p');
       $tgl_tutup_p = $this->input->post('tgl_tutup_p');
       $tgl_mulai_k = $this->input->post('tgl_mulai_k');
       $tgl_selesai_k = $this->input->post('tgl_selesai_k');
       $deskripsi = $this->input->post('deskripsi');
    //    $persyaratan = $this->input->post('persyaratan');

       $data = array(
           'nama_pelatihan' => $nama_pelatihan,
           'kuota_pendaftaran' => $kuota_pendaftaran,
           'kuota_kelas' => $kuota_kelas,
           'deskripsi' => $deskripsi,
           'tgl_pembukaan_pendaftaran' => $tgl_buka_p,
           'tgl_penutupan_pendaftaran' => $tgl_tutup_p,
           'tgl_kelas_mulai' => $tgl_mulai_k,
           'tgl_kelas_selesai' => $tgl_selesai_k,
           'created_by' => $user,
           'created_at' => $datenow
       );
       $this->db->where('id', $id);
       $update = $this->db->update('si_pelatihan', $data);

       if($update){
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('notice', 'Berhasil');
                $this->session->set_flashdata('message', 'Edit data berhasil');
                redirect(site_url('kelolapelatihan'));

            // $multi = array();
            // foreach($persyaratan as $key=>$value) {
            //     $multi[]  = array(
            //             'id'           => $id,
            //             'id_pelatihan' => $lastid,
            //             'id_syarat'    => $persyaratan[$key], 
            //         );
            // }
            // $multi_persyaratan = $this->db->insert_batch('si_pelatihan_syarat',$multi);
            // if($multi_persyaratan){
            //     $this->session->set_flashdata('info', 'success');
            //     $this->session->set_flashdata('notice', 'Berhasil');
            //     $this->session->set_flashdata('message', 'Tambah data berhasil');
            //     redirect(site_url('kelolapelatihan'));
            // }else{
            //     $this->db->where('id', $lastid);
            //     $this->db->delete('si_pelatihan'); 
            //     $this->session->set_flashdata('info', 'danger');
            //     $this->session->set_flashdata('notice', 'Gagal');
            //     $this->session->set_flashdata('message', 'Tambah data gagal');
            //     redirect(site_url('kelolapelatihan'));
            // }
        }else{
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('notice', 'Gagal');
            $this->session->set_flashdata('message', 'Edit data gagal');
            redirect(site_url('kelolapelatihan'));
        }
       
	}

    public function quesioner_add($id){
        $data['detail'] = $this->db->query("SELECT * FROM si_pelatihan WHERE id = '$id'")->row();
        $data['quesioner'] = $this->db->query("SELECT a.*, b.id as id_pelatihan_quesioner, b.id_pelatihan, b.id_quesioner, b.quesioner FROM si_quesioner a
        LEFT JOIN si_pelatihan_quesioner b ON b.id_quesioner = a.id AND b.id_pelatihan = '$id'
        
        GROUP BY a.id;")->result();
        
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/pelatihan/quesioner_add',$data);
    }

    public function quesioner($id){
        $data['detail'] = $this->db->query("SELECT * FROM si_pelatihan WHERE id = '$id'")->row();
        $data['quesioner'] = $this->M_kelolapelatihan->get_quesioner_all($id);
        
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/pelatihan/quesioner',$data);
    }

    public function aksi_tambah_quesioner()
	{
       $user = $this->session->userdata('id_pengguna');
       $datenow = date('Y-m-d H:i:s');
       $id_pelatihan = $this->input->post('id_pelatihan');
       $id_quesioner = $this->input->post('id_quesioner');
       $quesioner = $this->input->post('quesioner');
       
       $data = array();
       for($i = 0; $i < count($id_quesioner); $i++){
        $data[] = array(
            'id_pelatihan'  => $id_pelatihan,
            'id_quesioner'  => $id_quesioner[$i],
            'quesioner'     => $quesioner[$i],
            'created_by'	=> $user,
            'created_at'    => $datenow
        );
        }

    // echo  json_encode($data);
       $insert = $this->db->insert_batch('si_pelatihan_quesioner', $data);
       if($insert){
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('notice', 'Berhasil');
            $this->session->set_flashdata('message', 'Tambah data berhasil');
            redirect(site_url('kelolapelatihan/quesioner/'.$id_pelatihan));
        }else{
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('notice', 'Gagal');
            $this->session->set_flashdata('message', 'Tambah data gagal');
            redirect(site_url('kelolapelatihan/quesioner/'.$id_pelatihan));
        }
       
	}

    public function aksi_update_quesioner()
	{
       $user = $this->session->userdata('id_pengguna');
       $datenow = date('Y-m-d H:i:s');
       $id_pelatihan = $this->input->post('id_pelatihan');
       $id_pelatihan_quesioner = $this->input->post('id_pelatihan_quesioner');
       $quesioner = $this->input->post('quesioner');
       

       if($quesioner == ''){
           $k = 0;
       }else{
           $k = $quesioner;
       }
       
       $data = array();
       for($i = 0; $i < count($id_pelatihan_quesioner); $i++){
        
        $k = isset($_POST["quesioner"]) ? $quesioner : 0;

        $data[] = array(
            'id'            => $id_pelatihan_quesioner[$i],
            'quesioner'     => $quesioner[$i],
            // 'created_by'	=> $user,
            // 'updated_at'    => $datenow
        );
        }

    // echo  json_encode($data);
       $update = $this->db->update_batch('si_pelatihan_quesioner',$data, 'id'); 
       if($update){
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('notice', 'Berhasil');
            $this->session->set_flashdata('message', 'Update data berhasil');
            redirect(site_url('kelolapelatihan/quesioner/'.$id_pelatihan));
        }else{
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('notice', 'Gagal');
            $this->session->set_flashdata('message', 'Update data gagal');
            redirect(site_url('kelolapelatihan/quesioner/'.$id_pelatihan));
        }
       
	}

    public function persyaratan($id){
        $data['detail'] = $this->db->query("SELECT * FROM si_pelatihan WHERE id = '$id'")->row();
        $data['alldata'] = $this->db->query("SELECT a.*, b.deskripsi FROM si_pelatihan_syarat a
        INNER JOIN si_syarat b ON b.id = a.id_syarat
        WHERE id_pelatihan = '$id'")->result();
        $data['persyaratan'] = $this->db->query("SELECT * FROM si_syarat")->result();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/pelatihan/persyaratan',$data);
    }

    public function aksi_tambah_persyaratan()
	{
       $user = $this->session->userdata('id_pengguna');
       $datenow = date('Y-m-d H:i:s');
       $id_pelatihan = $this->input->post('id_pelatihan');
       $persyaratan = $this->input->post('persyaratan');

            $multi = array();
            foreach($persyaratan as $key=>$value) {
                $multi[]  = array(
                        'id_pelatihan' => $id_pelatihan,
                        'id_syarat' => $persyaratan[$key], 
                        'created_at' => $datenow,
                        'created_by' => $user,
                    );
            }
            $multi_persyaratan = $this->db->insert_batch('si_pelatihan_syarat',$multi);
            if($multi_persyaratan){
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('notice', 'Berhasil');
                $this->session->set_flashdata('message', 'Tambah data berhasil');
                redirect(site_url('kelolapelatihan/persyaratan/'.$id_pelatihan));
            }else{
                $this->db->where('id', $lastid);
                $this->db->delete('si_pelatihan'); 
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('notice', 'Gagal');
                $this->session->set_flashdata('message', 'Tambah data gagal');
                redirect(site_url('kelolapelatihan/persyaratan/'.$id_pelatihan));
            }
       
	}

    public function hapus_persyaratan($id, $id_pelatihan)
    {
        $this->db->where('id', $id);
        $del = $this->db->delete('si_pelatihan_syarat');
        if ($del) { 
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Hapus data berhasil');
            redirect(site_url('kelolapelatihan/persyaratan/'.$id_pelatihan));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Hapus data gagal ');
            redirect(site_url('kelolapelatihan/persyaratan/'.$id_pelatihan));
        }
    }

    public function cetak_pendaftar($id){
        $data['quesioner'] = $this->M_kelolapelatihan->get_quesioner($id);
        $data['detail'] = $this->db->query("SELECT * FROM si_pelatihan WHERE id = '$id'")->row();
        $data['alldata'] = $this->M_kelolapelatihan->get_pendaftar_pelatihan($id);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/pelatihan/cetak_pendaftar',$data);
    }

    public function pendaftar($id){
        $data['detail'] = $this->db->query("SELECT * FROM si_pelatihan WHERE id = '$id'")->row();
        $data['alldata'] = $this->M_kelolapelatihan->get_pendaftar_pelatihan($id);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/pelatihan/pendaftar',$data);
    }

    public function detail_pendaftar($id){
        
        $data_pendaftaran = $this->M_kelolapelatihan->get_detail_pendaftaran($id);
        $data['data_pendaftaran'] = $data_pendaftaran;
        $data['data_pendaftar'] = $this->M_kelolapelatihan->detail_pendaftar($data_pendaftaran->id_user);
        $data['data_riwayat_Pelatihan_pendaftar'] = $this->M_welcome->get_riwayat_pelatihan($data_pendaftaran->id_user);
        $data['jawaban_quesioner'] = $this->M_kelolapelatihan->detail_jawaban_quesioner($data_pendaftaran->id);
        $data['status_pendaftaran'] = $this->M_kelolapelatihan->status_pendaftaran();
        $data['detail'] = $this->db->query("SELECT * FROM si_pelatihan WHERE id = '$data_pendaftaran->id_pelatihan'")->row();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/pelatihan/pendaftar_detail',$data);
    }

    public function testimoni($id){
        $data['detail'] = $this->db->query("SELECT * FROM si_pelatihan WHERE id = '$id'")->row();
        $data['alldata'] = $this->M_kelolapelatihan->get_pendaftar_pelatihan($id);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/pelatihan/testimoni',$data);
    }

    public function tampilkan_testimoni($id, $pelatihan)
    {
        $data = array(
            'status_testimoni' => 1
        );
        $this->db->where('id', $id);
        $edit = $this->db->update('si_pelatihan_pendaftaran', $data);
        if ($edit) { 
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Tampilkan data berhasil');
            redirect(site_url('kelolapelatihan/testimoni/'.$pelatihan));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Tampilkan data gagal ');
            redirect(site_url('kelolapelatihan/testimoni/'.$pelatihan)); 
        }
    }

    public function hidden_testimoni($id, $pelatihan)
    {
        $data = array(
            'status_testimoni' => 0
        );
        $this->db->where('id', $id);
        $edit = $this->db->update('si_pelatihan_pendaftaran', $data);
        if ($edit) { 
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Tampilkan data berhasil');
            redirect(site_url('kelolapelatihan/testimoni/'.$pelatihan));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Tampilkan data gagal ');
            redirect(site_url('kelolapelatihan/testimoni/'.$pelatihan)); 
        }
    }

    public function hapus($id)
    {
        $row = $this->M_kelolapelatihan->soft_del($id);
        if ($row) { 
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Hapus data berhasil');
            redirect(site_url('kelolapelatihan'));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Hapus data gagal ');
            redirect(site_url('kelolapelatihan'));
        }
    }

    

    public function update_status_pendaftaran(){
       $user = $this->session->userdata('id_pengguna');
       $datenow = date('Y-m-d H:i:s');
       $id_pendaftaran = $this->input->post('id_pendaftaran');
       $id_pendaftar = $this->input->post('id_pendaftar');
       $status = $this->input->post('status');
       $alasan = $this->input->post('alasan');
       $data = array(
           'status'     => $status,
           'keterangan' => $alasan
       );
       $this->db->where('id', $id_pendaftaran);
       $update = $this->db->update('si_pelatihan_pendaftaran',$data );
       if ($update) { 
            $data_pendaftaran = $this->M_kelolapelatihan->get_detail_pendaftaran($id_pendaftaran);
            $pendaftar = $this->M_kelolapelatihan->detail_pendaftar($id_pendaftar);
            $pesan = 'Informasi%20BLK%20Semarang%201:%0AAnda%20_*'.status_pendaftaran($status).'*_%20Pendaftaran Pelatihan%20'.nama_pelatihan($data_pendaftaran->id_pelatihan).'%0APada%20BLK%20Semarang 1%0AKunjungi%20Website%20Official%20BLK%20Semarang%20Sekarang%20di:%20www.blksmg1.disnakertrans.jatengprov.go.id';
            // $pesan = 'Informasi BLK Semarang 1:<br/> Anda '.status_pendaftaran($status).' Pendaftaran Pelatihan '. nama_pelatihan($data_pendaftaran->id_pelatihan).' Pada BLK Semarang 1<br/>Kunjungi <a href="http://blksmg1.disnakertrans.jatengprov.go.id/">Website Official BLK Semarang 1</a> Sekarang.';
            
            echo $pesan;
            $data = array(
                'for_user' => $id_pendaftar,
                'status'    => 0,
                'info'      => 'Update Informasi Pendaftaran Pelatihan',
                'link'      => 'publik/riwayat',
                'created_by'=> $user,
                'created_at'=> $datenow
            );
            $this->db->insert('si_notifikasi',$data );
            redirect('https://api.whatsapp.com/send?phone='.$pendaftar->wa.'&text='.$pesan);
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Update Satatus Gagal ');
            redirect(site_url('kelolapelatihan/detail_pendaftar/'.$id_pendaftaran ));
        }


    }


    public function update_status_pendaftaran_save(){
        $user = $this->session->userdata('id_pengguna');
        $datenow = date('Y-m-d H:i:s');
        $id_pendaftaran = $this->input->post('id_pendaftaran');
        $id_pendaftar = $this->input->post('id_pendaftar');
        $status = $this->input->post('status');
        $alasan = $this->input->post('alasan');
        $data = array(
            'status'     => $status,
            'keterangan' => $alasan
        );
        $this->db->where('id', $id_pendaftaran);
        $update = $this->db->update('si_pelatihan_pendaftaran',$data );
        if ($update) { 
             $data_pendaftaran = $this->M_kelolapelatihan->get_detail_pendaftaran($id_pendaftaran);
             $pendaftar = $this->M_kelolapelatihan->detail_pendaftar($id_pendaftar);
             // $pelatihan = $this->db->query("SELECT * FROM si_pelatihan WHERE id = '$data_pendaftaran->id_pelatihan'")->row();
 
             $pesan = 'Informasi%20BLK%20Semarang%201:%0AAnda%20'.status_pendaftaran($status).'%20Pendaftaran Pelatihan%20'. nama_pelatihan($data_pendaftaran->id_pelatihan).'%20Pada%20BLK%20Semarang 1%0AKunjungi%20<a href="http://blksmg1.disnakertrans.jatengprov.go.id/">Website%20Official%20BLK%20Semarang%201</a>%20Sekarang.';
             // $pesan = 'Informasi BLK Semarang 1:<br/> Anda '.status_pendaftaran($status).' Pendaftaran Pelatihan '. nama_pelatihan($data_pendaftaran->id_pelatihan).' Pada BLK Semarang 1<br/>Kunjungi <a href="http://blksmg1.disnakertrans.jatengprov.go.id/">Website Official BLK Semarang 1</a> Sekarang.';
             
             echo $pesan;
 
             // send_wa($pendaftar->wa, $pesan)
 
             // $curl = curl_init();
             // $token = "WDi0oSGJnw3M3oiyOrkMQD9zCRMBIbZpaQN24L5CbZclJ7Cin8glHEneuA6OOHBo";
             // $data = [
             //     'phone' => '085641903904',
             //     'message' => 'hellow world',
             //     'secret' => false, // or true
             //     'priority' => false, // or true
             // ];
             
             // curl_setopt($curl, CURLOPT_HTTPHEADER,
             //     array(
             //         "Authorization: $token",
             //     )
             // );
             // curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
             // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
             // curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
             // curl_setopt($curl, CURLOPT_URL, "https://sambi.wablas.com/api/send-message");
             // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
             // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
             // $result = curl_exec($curl);
             // curl_close($curl);
             
             // echo "<pre>";
             // print_r($result);
 
             
 
 
             // $data = array(
             //     'for_user' => $id_pendaftar,
             //     'status'    => 0,
             //     'info'      => 'Update Informasi Pendaftaran Pelatihan',
             //     'link'      => 'publik/riwayat',
             //     'created_by'=> $user,
             //     'created_at'=> $datenow
             // );
             // $this->db->insert('si_notifikasi',$data );
             // $this->session->set_flashdata('info', 'success');
             // $this->session->set_flashdata('message', 'Update  Status Berhasil');
             // redirect(site_url('kelolapelatihan/detail_pendaftar/'.$id_pendaftaran ));
         } else {
             $this->session->set_flashdata('info', 'danger');
             $this->session->set_flashdata('message', 'Update Satatus Gagal ');
             redirect(site_url('kelolapelatihan/detail_pendaftar/'.$id_pendaftaran ));
         }
 
 
     }

}?>