<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
        $this->load->model('M_kelolauser');
        $this->load->model('M_dashboard');
        is_login();

	}
	
	public function index()
	{   
        $data['pelatihan']=$this->db->query("SELECT COUNT(id) as a FROM si_pelatihan WHERE deleted_at IS NULL")->row()->a;
        $data['peserta']=$this->db->query("SELECT COUNT(id) as a FROM si_pelatihan_pendaftaran WHERE deleted_at IS NULL AND `status` = 2;")->row()->a;
        $data['user']=$this->db->query("SELECT COUNT(id_pengguna) as a FROM si_user WHERE deleted_at IS NULL")->row()->a;
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/dashboard', $data);
        
	}

	public function edit_password()
    {
		$id = $this->session->userdata('id_pengguna');
        $row = $this->M_kelolauser->get_by_id($id);
        if ($row) {
                $data['detail']    = $row;
                $this->load->view('backend/template/head');
                $this->load->view('backend/template/header');
                $this->load->view('backend/template/sidebar');
                $this->load->view('backend/user/form_edit_password',$data);
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'waduh, gagal ');
            redirect(site_url('dashboard'));
        }
	}
	
	public function aksi_edit_password()
    {
            $id = $this->input->post('id_user',TRUE);
            $edit = array(
                'password'              => md5($this->input->post('password')),
            );
            $data = $this->M_kelolauser->update($id, $edit);
            //echo json_encode($data);
            if($data){
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Edit ');
            redirect(site_url('dashboard'));
            }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'waduh, gagal ');
                redirect(site_url('dashboard/edit_password'));
            }
          
    }
	
}
