<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kelolapesan extends CI_Controller
{


    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('M_kelolapesan');
    }

    public function index()
	{
        $data['alldata'] = $this->M_kelolapesan->get_all();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/pesan/index',$data);
	}

    public function tambah()
	{
        $data['persyaratan'] = $this->db->query("SELECT * FROM si_syarat")->result();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/pesan/form_pesan',$data);
	}

    public function aksi_tambah()
	{
       $user = $this->session->userdata('id_pengguna');
       $datenow = date('Y-m-d H:i:s');
       $pertanyaan = $this->input->post('pertanyaan');
       $jawaban = $this->input->post('jawaban');

       $data = array(
           'pertanyaan' => $pertanyaan,
           'jawaban'    => $jawaban,
           'created_by' => $user,
           'created_at' => $datenow
       );
       $insert = $this->db->insert('si_pesan', $data);

       if($insert){
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('notice', 'Success');
            $this->session->set_flashdata('message', 'Tambah data Berhasil');
            redirect(site_url('kelolapesan'));
        }else{
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('notice', 'Gagal');
            $this->session->set_flashdata('message', 'Tambah data gagal');
            redirect(site_url('kelolapesan'));
        }
       
	}

    public function edit($id){
        $data['detail'] = $this->db->query("SELECT * FROM si_pesan WHERE id = '$id'")->row();
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/pesan/edit',$data);
    }

    public function aksi_edit()
    {
            $id = $this->input->post('id',TRUE);
            $edit = array(
                'id'          => $id,
                'pertanyaan'           => $this->input->post('pertanyaan'),
                'jawaban'           => $this->input->post('jawaban'),
            );
            
            $data = $this->M_kelolapesan->update($id, $edit);
            //echo json_encode($data);
            if($data){
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Edit ');
            redirect(site_url('kelolapesan'));
            }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'waduh, gagal ');
                redirect(site_url('kelolapesan/edit/'.$id));
            }
          
    }

    public function hapus($id)
    {
        $row = $this->M_kelolapesan->soft_del($id);
        if ($row) { 
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Hapus data berhasil');
            redirect(site_url('kelolapesan'));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Hapus data gagal ');
            redirect(site_url('kelolapesan'));
        }
    }

}?>