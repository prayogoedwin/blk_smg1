<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_kelolapesan extends CI_Model
{

    public $table = 'si_form';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    function get_all()
    {
        $get = $this->db->query("SELECT a.*
        FROM si_form a 
        WHERE a.deleted_at IS NULL ORDER BY id DESC;");
        return $get->result();
    }
    
   
     // get data by id
     function get_by_id($id)
     {
        $get = $this->db->query("SELECT a.* FROM si_faq a 
        WHERE a.id = '$id'");
        return $get->row();
     }

    


    // get data by id
    // function get_by_id($id)
    // {
    //     $this->db->where($this->id, $id);
    //     return $this->db->get($this->table)->row();
    // }

    // insert data
    function insert($data)
    {
        $add = $this->db->insert($this->table, $data);
        if($add){
            return true;
        }else{
            return false;
        }
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $edit = $this->db->update($this->table, $data);
        if($edit){
            return true;
        }else{
            return false;
        }
    }

    


    function soft_del($id)
    {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s')
            );

        $this->db->where('id', $id);
        $reset = $this->db->update('si_form', $data);
        if($reset){
            return true;
        }else{
            return false;
        }
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Tbl_banner_model.php */
/* Location: ./application/models/Tbl_banner_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-21 07:00:03 */
/* http://harviacode.com */
