<?php

    class M_welcome extends CI_Model{

        public function login($user, $pass){
            $this->db->select('*');
            $this->db->where('username', $user);
            $this->db->where('password', $pass);
            // $this->db->where('aktif', 0);
            $this->db->where('deleted_at', NULL);
            $this->db->where('role',1);
            $log = $this->db->get('si_user');
           
            if ($log->num_rows()>0){
                // $loguser = $log->row_array();
                // $this->session->set_userdata($loguser);
                $loguser = $log->row();
                $this->session->set_userdata('id_pengguna', $loguser->id_pengguna);
                return true;
            }else{
                return false;
            }
        }

        public function login_peserta($user, $pass){
            $this->db->select('*');
            $this->db->where('username', $user);
            $this->db->where('password', $pass);
            // $this->db->where('aktif', 0);
            $this->db->where('deleted_at', NULL);
            $this->db->where('role',2);
            $log = $this->db->get('si_user');
           
            if ($log->num_rows()>0){
                // $loguser = $log->row_array();
                // $this->session->set_userdata($loguser);
                $loguser = $log->row();
                $this->session->set_userdata('id_pengguna', $loguser->id_pengguna);
                return true;
            }else{
                return false;
            }
        }

        function pelatihan()
        {
            $get = $this->db->query("SELECT a.*
            FROM si_pelatihan a 
            WHERE a.deleted_at IS NULL ORDER BY id DESC;");
            return $get->result();
        }

        function pelatihan_quesioner_full($id)
        {
            $get = $this->db->query("SELECT a.*, b.id as id_pelatihan_quesioner, b.id_pelatihan, b.id_quesioner, b.quesioner, c.nama as tipe FROM si_quesioner a
            LEFT JOIN si_pelatihan_quesioner b ON b.id_quesioner = a.id AND b.id_pelatihan = '$id'
            INNER JOIN si_quesioner_tipe c ON a.tipe_quesioner = c.id
            GROUP BY a.id;");
            return $get->result();
        }

        function pelatihan_quesioner_full_without($id, $tipe)
        {
            $get = $this->db->query("SELECT a.*, b.id as id_pelatihan_quesioner, b.id_pelatihan, b.id_quesioner, b.quesioner, c.nama as tipe FROM si_quesioner a
            LEFT JOIN si_pelatihan_quesioner b ON b.id_quesioner = a.id AND b.id_pelatihan = '$id'
            INNER JOIN si_quesioner_tipe c ON a.tipe_quesioner = c.id
            WHERE tipe_quesioner != '$tipe'
            GROUP BY a.id;");
            return $get->result();
        }

        function pelatihan_quesioner($id, $tipe)
        {
            $get = $this->db->query("SELECT a.*, b.id as id_pelatihan_quesioner, b.id_pelatihan, b.id_quesioner, b.quesioner, c.nama as tipe FROM si_quesioner a
            LEFT JOIN si_pelatihan_quesioner b ON b.id_quesioner = a.id AND b.id_pelatihan = '$id'
            INNER JOIN si_quesioner_tipe c ON a.tipe_quesioner = c.id
            WHERE tipe_quesioner = '$tipe'
            GROUP BY a.id;");
            return $get->result();
        }   

        function pelatihan_quesioner_show($id, $tipe)
        {
            $get = $this->db->query("SELECT a.*, b.id as id_pelatihan_quesioner, b.id_pelatihan, b.id_quesioner, b.quesioner, c.nama as tipe FROM si_quesioner a
            LEFT JOIN si_pelatihan_quesioner b ON b.id_quesioner = a.id AND b.id_pelatihan = '$id'
            INNER JOIN si_quesioner_tipe c ON a.tipe_quesioner = c.id
            WHERE tipe_quesioner = '$tipe'
            AND quesioner = 1
            GROUP BY a.id;");
            return $get->result();
        } 


        function get_riwayat_pelatihan($id_user){

            $get = $this->db->query("SELECT a.*, b.id as id_pelatihan, b.nama_pelatihan, b.`tgl_pembukaan_pendaftaran`, b.`tgl_penutupan_pendaftaran`, b.`tgl_kelas_mulai`, b.`tgl_kelas_selesai` ,b.form_testimoni
            FROM si_pelatihan_pendaftaran a 
            INNER JOIN si_pelatihan b ON a.id_pelatihan = b.id WHERE a.id_user = '$id_user' AND a.deleted_at IS NULL ");
            return $get->result();

        }
        
        

        
    }

?>