<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_kelolapelatihan extends CI_Model
{

    public $table = 'si_pelatihan';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    function get_all()
    {
        $get = $this->db->query("SELECT a.*
        FROM si_pelatihan a 
        WHERE a.deleted_at IS NULL ORDER BY id DESC;");
        return $get->result();
    }

    function get_all_limit($limit)
    {
        $get = $this->db->query("SELECT a.*
        FROM si_pelatihan a 
        WHERE a.deleted_at IS NULL ORDER BY id DESC
        LIMIT $limit
        ;");
        return $get->result();
    }

    function get_all_show_limit($limit)
    {
        $get = $this->db->query("SELECT a.*
        FROM si_pelatihan a 
        WHERE a.deleted_at IS NULL AND a.status = 1 ORDER BY id DESC
        LIMIT $limit
        ;");
        return $get->result();
    }

    function get_all_show()
    {
        $get = $this->db->query("SELECT a.*
        FROM si_pelatihan a 
        WHERE a.deleted_at IS NULL AND a.status = 1 ORDER BY id DESC;");
        return $get->result();
    }
    
   
     // get data by id
     function get_by_id($id)
     {
        $get = $this->db->query("SELECT * FROM si_pelatihan a 
        WHERE a.id = '$id' ");
        return $get->row();
     }

    


    // get data by id
    // function get_by_id($id)
    // {
    //     $this->db->where($this->id, $id);
    //     return $this->db->get($this->table)->row();
    // }

    // insert data
    function insert($data)
    {
        $add = $this->db->insert($this->table, $data);
        if($add){
            return true;
        }else{
            return false;
        }
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $edit = $this->db->update($this->table, $data);
        if($edit){
            return true;
        }else{
            return false;
        }
    }


    function soft_del($id)
    {
        $datenow = date('Y-m-d H:i:s');
        $data = array(
            'deleted_at' => $datenow
            );
        $this->db->where($this->id, $id);
        $reset = $this->db->update($this->table, $data);
        if($reset){
            return true;
        }else{
            return false;
        }
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function get_pendaftar_pelatihan($id){
        return $this->db->query("SELECT a.*, b.nama, b.nik, b.kota_domisili, b.hp, b.wa, c.name as kota FROM si_pelatihan_pendaftaran a
        INNER JOIN si_user b ON a.id_user = b.id_pengguna
        INNER JOIN si_kota c ON b.kota_domisili = c.id
        WHERE a.id_pelatihan = '$id'")->result();
    }

    function get_detail_pendaftaran($id_pelatihan_pendaftaran){
        return $this->db->query("SELECT a.* FROM si_pelatihan_pendaftaran a
        WHERE a.id = '$id_pelatihan_pendaftaran'")->row();
    }

    function detail_pendaftar($id_user){
        return $this->db->query("SELECT b.*, c.name as kota FROM si_user b
        INNER JOIN si_kota c ON b.kota_domisili = c.id
        WHERE b.id_pengguna = '$id_user';")->row();
    }

    function detail_jawaban_quesioner($id_pelatihan_pendaftaran){
        return $this->db->query("SELECT a.*, b.*, c.*  FROM si_pelatihan_pendaftaran_quesioner a 
        INNER JOIN si_pelatihan_quesioner b ON b.id = a.id_pelatihan_quesioner
        INNER JOIN si_quesioner c ON b.id_quesioner = c.id
        WHERE a.id_pelatihan_pendaftaran = '$id_pelatihan_pendaftaran'
        AND b.quesioner = 1
        ")->result();
        
    }

    function get_quesioner_all($id_pelatihan){
        return $this->db->query("SELECT a.*, b.id as id_pelatihan_quesioner, b.id_pelatihan, b.id_quesioner, b.quesioner, c.nama as tipe FROM si_quesioner a
        LEFT JOIN si_pelatihan_quesioner b ON b.id_quesioner = a.id AND b.id_pelatihan = '$id_pelatihan'
        INNER JOIN si_quesioner_tipe c ON a.tipe_quesioner = c.id
        GROUP BY a.id;
        ")->result();
        
    }

    function get_quesioner($id_pelatihan){
        return $this->db->query("SELECT a.*, b.id as id_pelatihan_quesioner, b.id_pelatihan, b.id_quesioner, b.quesioner, c.nama as tipe FROM si_quesioner a
        LEFT JOIN si_pelatihan_quesioner b ON b.id_quesioner = a.id AND b.id_pelatihan = '$id_pelatihan'
        INNER JOIN si_quesioner_tipe c ON a.tipe_quesioner = c.id
        WHERE b.quesioner = 1
        GROUP BY a.id;
        ")->result();
        
    }

    function detail_jawaban_quesioner_by_pelatihan($id_pelatihan, $id_user){
        return $this->db->query("SELECT a.*, b.*, c.*  FROM si_pelatihan_pendaftaran_quesioner a 
		INNER JOIN si_pelatihan_pendaftaran d ON a.id_pelatihan_pendaftaran = d.id
        INNER JOIN si_pelatihan_quesioner b ON b.id = a.id_pelatihan_quesioner
        INNER JOIN si_quesioner c ON b.id_quesioner = c.id
        WHERE b.id_pelatihan = '$id_pelatihan' AND d.id_user = '$id_user'
        AND b.quesioner = 1
        ")->result();
        
    }

    function status_pendaftaran(){
        return $this->db->query("SELECT a.*FROM si_status_pendaftaran a")->result();
    }

}

/* End of file Tbl_banner_model.php */
/* Location: ./application/models/Tbl_banner_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-21 07:00:03 */
/* http://harviacode.com */
