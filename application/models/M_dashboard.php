<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_dashboard extends CI_Model
{

    public $table = 'datadasar';
    public $id = 'id_datadasar';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    

    
    function target_realisasi_capaian($tahun)
    {
        $get = $this->db->query("SELECT c.nama_kota AS kota, 
        SUM(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, a.anggaran_t,0)) AS anggaran_tahun_pelaporan,
        SUM(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, realisasi,0)) AS realisasi_anggaran_tahun_pelaporan,
        SUM(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, a.jumlah_pembilang,0)) /SUM(IF(a.tahun = 2020 AND a.deleted_at IS NULL, a.jumlah_penyebut,0)) * 100 AS realisasi_capaian,
        SUM(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, a.realisasi,0)) /SUM(IF(a.tahun = 2020 AND a.deleted_at IS NULL, a.anggaran_t,0)) * 100 AS realisasi_anggaran        
        FROM targetcapaian a
        RIGHT JOIN users b ON a.id_user = b.id_user
        INNER JOIN kota c ON b.id_kota = c.id_kota
        GROUP BY b.id_kota;");
        return $get->result();
    }

   
    function kendala($tahun)
    {
        $get = $this->db->query("SELECT c.nama_jenis_spm as spm_s,
        COUNT(IF(a.tahun = '$tahun' AND a.deleted_at IS NULL, a.kendala, 0)) as jml_kendala
        FROM targetcapaian a
        RIGHT JOIN users b ON a.id_user = b.id_user
        INNER JOIN jenis_spm c ON b.id_pengampu_spm = c.id_jenis_spm
        WHERE b.id_pengampu_spm != 1
        GROUP BY b.id_pengampu_spm;");
        return $get->result();
    }


}?>