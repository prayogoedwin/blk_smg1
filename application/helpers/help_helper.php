<?php

function is_login(){
    $ci = get_instance();
    if($ci->session->userdata('id_pengguna') == null){
        redirect('portal');
    }else{
        return true;
    }
}

function get_user($id){
    $ci = get_instance();
    $user = $ci->db->query("SELECT * FROM si_user WHERE id_pengguna = '$id'")->row();
    return $user;
}

function nama_kota($kota){
    $ci = get_instance();
    $x = $ci->db->query("SELECT name FROM tbl_kota WHERE id = $kota")->row();
    return $x->name;
}

function nama_spm($spm){
    $ci = get_instance();
    $x = $ci->db->query("SELECT * FROM jenis_spm WHERE id_jenis_spm = $spm")->row();
    return $x;
}

function nama_kec($kec){
    $ci = get_instance();
    $y = $ci->db->query("SELECT name FROM tbl_kecamatan WHERE id = $kec")->row();
    return $y->name;
}

function nama_kel($kel){
    $ci = get_instance();
    $z = $ci->db->query("SELECT name FROM tbl_desa WHERE id = $kel")->row();
    return $z->name;
}

function ClientPost($endpoint, $_param){

    //$url = "http://ayokitakerjatraining.kemnaker.go.id/api/".$endpoint;
    $url = $endpoint;
    $postData = '';
    //create name value pairs seperated by &
    foreach($_param as $k => $v) 
    { 
      $postData .= $k . '='.$v.'&'; 
    }
    rtrim($postData, '&');


    $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL, $url);
    // curl_setopt($ch, CURLOPT_POST, 1);
    // curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);  
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    // curl_setopt($ch, CURLOPT_HEADER, false);
    // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    // curl_setopt($ch, CURLOPT_ENCODING, '');
    
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPGET, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false);
    curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2 );
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);

    // curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    //     'Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkZXRhaWwiOnsidXNlcl9pZCI6ImphdGVuZyIsInBhc3N3b3JkIjoiIiwibmFtYSI6IkRpbmFzIFRlbmFnYSBLZXJqYSBkYW4gVHJhbnNtaWdyYXNpIFByb3ZpbnNpIEphd2EgVGVuZ2FoIiwiZW1haWwiOiJidXJzYWtlcmphamF0ZW5nQGdtYWlsLmNvbSIsImlkX2dyb3VwIjoiQURNSU5fUFJPViIsImFrdGlmIjoiMSIsInVwZGF0ZV9ieSI6IjEiLCJ1cGRhdGVfZGF0ZSI6IjIwMTktMTItMTkiLCJpZCI6IiJ9LCJrYW50b3IiOmZhbHNlLCJsb2dpbiI6dHJ1ZSwidGltZXN0YW1wIjoxNTgxNTcyNzg5fQ.jM1HYW3C90LBtWK5Tkydjb-0cY6fVMVodmaqNZYnIK4'
    // )
    
    $response = curl_exec($ch);
    $err = curl_error($ch);
    
    curl_close($ch);

    if ($err) {
        // echo "cURL Error #:" . $err;
        return $err;
    } else {
        // $hsl = json_decode($response, TRUE);
        // $status = $hsl['status'];
        // if($status){
        //     // return TRUE;
        //     return $response;
        // } else {
        //     //return FALSE;
        //     return $hsl;
        // }

        // header('Content-Type: application/json');
        // echo $response;

        return $response;
    }
}

function cek_persyaratan($id_pelatihan, $id_syarat){
    $ci = get_instance();
    $z = $ci->db->query("SELECT id FROM si_pelatihan_syarat WHERE id_pelatihan = '$id_pelatihan' AND id_syarat='$id_syarat'")->row();
    if($z){
        if($z->id != ''){
            return 'selected';
        }else{
            return '';
        }
    }else{
        return '';
    }
}

function cek_quesioner($id_pelatihan){
    $ci = get_instance();
    $z = $ci->db->query("SELECT COUNT(id) as a FROM si_pelatihan_quesioner WHERE id_pelatihan = '$id_pelatihan'")->row();
    return $z->a;
}

function syarat($id_pelatihan){
    $ci = get_instance();
    return $ci->db->query("SELECT a.*, b.deskripsi FROM si_pelatihan_syarat a
        INNER JOIN si_syarat b ON b.id = a.id_syarat
        WHERE id_pelatihan = '$id_pelatihan'")->result();
}



function nama_pelatihan($id_pelatihan){
    $ci = get_instance();
    $z = $ci->db->query("SELECT nama_pelatihan as a FROM si_pelatihan WHERE id = '$id_pelatihan'")->row();
    return $z->a;
}

function data_user($id){
    $ci = get_instance();
    $z = $ci->db->query("SELECT * FROM si_user WHERE id_pengguna = '$id'")->row();
    if($z){
        return $z;
    }else{
        return '';
    }
    
}

function data_pilihan($id){
    $ci = get_instance();
    $z = $ci->db->query("SELECT * FROM si_quesioner_pilihan WHERE id_quesioner = '$id'")->result();
    if($z){
        return $z;
    }else{
        return '';
    }
    
}

function jawaban_pilihan($id){
    $ci = get_instance();
    $z = $ci->db->query("SELECT * FROM si_quesioner_pilihan WHERE id = '$id'")->row();
    if($z){
        return $z->pilihan;
    }else{
        return '';
    }
    
}

function status_pendaftaran($val){
    switch ($val) {
        case '0':
            //sudah menambahkan tapi belum di ajukan, blm tampil di admin
            return 'Menunggu';
            break;
        case '1':
            //yang sudah di ajukan, tampil di admin
            return 'Lolos Administrasi';
            break;
        case '2':
            //yang sudah di acc admin
            return 'Lolos Pelatihan';
            break;
        case '3':
            //yang diminta revisi oleh admin
            return 'Tidak Lolos';
            break;
        default:
            return '-';
            break;
    }

}

function pendidikan($val){
    switch ($val) {
        case '1':
            //sudah menambahkan tapi belum di ajukan, blm tampil di admin
            return 'Tidak Sekolah';
            break;
        case '2':
            //yang sudah di ajukan, tampil di admin
            return 'SD atau sederajat';
            break;
        case '3':
            //yang sudah di acc admin
            return 'SMP atau sederajat';
            break;
        case '4':
            //yang diminta revisi oleh admin
            return 'SMA / SMK atau sederajat';
            break;
        case '5':
            //yang diminta revisi oleh admin
            return 'D3 / D4 / S1';
            break;
       case '6':
            //yang diminta revisi oleh admin
            return 'S2 / S3';
            break;     
        default:
            return '-';
            break;
    }

}

function tipe_jawaban($val){
    switch ($val) {
        case '1':
            //yang sudah di ajukan, tampil di admin
            return '(Pilihan)';
            break;
        case '2':
            //yang sudah di acc admin
            return '(Ya/Tidak)';
            break;
        case '43':
            //yang diminta revisi oleh admin
            return '(Checkox)';
            break; 
        default:
            return '(Free Text)';
            break;
    }

}

function count_notif($id){
    $ci = get_instance();
    $z = $ci->db->query("SELECT COUNT(id) as a FROM si_notifikasi WHERE for_user = '$id' AND `status` = 0 ")->row();
    if($z){
        return $z->a;
    }else{
        return '';
    }
}

function detail_jawaban_quesioner_by_pelatihan($id_pelatihan, $id_user){
    $ci = get_instance();
    return $ci->db->query("SELECT a.*, b.*, c.*  FROM si_pelatihan_pendaftaran_quesioner a 
    INNER JOIN si_pelatihan_pendaftaran d ON a.id_pelatihan_pendaftaran = d.id
    INNER JOIN si_pelatihan_quesioner b ON b.id = a.id_pelatihan_quesioner
    INNER JOIN si_quesioner c ON b.id_quesioner = c.id
    WHERE b.id_pelatihan = '$id_pelatihan' AND d.id_user = '$id_user'
    AND b.quesioner = 1
    ")->result();
}

function send_wa($no, $pesan){
    $curl = curl_init();
            $token = "WDi0oSGJnw3M3oiyOrkMQD9zCRMBIbZpaQN24L5CbZclJ7Cin8glHEneuA6OOHBo";
            $data = [
                'phone' => $no,
                'message' => $pesan,
                'secret' => false, // or true
                'priority' => false, // or true
            ];
            
            curl_setopt($curl, CURLOPT_HTTPHEADER,
                array(
                    "Authorization: $token",
                )
            );
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($curl, CURLOPT_URL, "https://sambi.wablas.com/api/send-message");
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            $result = curl_exec($curl);
            curl_close($curl);
            
            echo "<pre>";
            print_r($result);
}

function cek_keikutsertaan($id_user,$id_pelatihan){
    $ci = get_instance();
    $z = $ci->db->query("SELECT COUNT(id) as a FROM si_pelatihan_pendaftaran WHERE id_user = '$id_user' AND id_pelatihan = '$id_pelatihan' ")->row();
    if($z){
        return $z->a;
    }else{
        return '';
    }
}





?>
